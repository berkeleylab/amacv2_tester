import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import csv

v_per_count = 5.0/65535.0	#LT2666 DAC set in 5V range
divider = 3.4/5.0			# ~2/3 divider made from RB15/RB12
dac_steps_in_nA = v_per_count * divider * 1e6

filename = 'OutputCM_AMAC_sln10_200runs_plot.csv'
amac_num = 10				# serial number
VDD = 1.23					# in V, determined via calibration
bgmV = 658					# in mV

point1_start = 0			# Dependent on size of data
point1_end = 199
point2_start = 3200
point2_end = 3399
point3_start = 7400
point3_end = 7599

matplotlib.rcParams['font.size'] = 15
matplotlib.rcParams['figure.figsize']=(12,8)
matplotlib.rcParams['legend.fontsize']=20 

# Import data
current = []
amac_counts = []
with open(filename, 'rb') as csvfile:	# generated from OutputCM_load_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		current.append(float(column[1]))
		amac_counts.append(float(column[2]))
		
# Find fit of AMACv2 counts v. current line; gives gain 
m, b = np.polyfit(current, amac_counts, 1)
fitted_amac_counts = np.multiply(current, m) + b
mA_per_count = 1.0/m * 1e3
fit_data = "m = %.3G count/A " %m + "(%.3G mA/count), " %mA_per_count + "b = %.3G counts" %b

offset_current = np.mean(amac_counts[point1_start:point1_end])	# mean of the first point (offset)

# Plot overlayed data
plt.subplot(3,3,(1,6))
plt.plot(current, amac_counts, '.')
plt.plot(current, fitted_amac_counts, '-', label="fit: " + fit_data)
plt.xlabel('Test current (A)')
plt.ylabel('AMAC counts')
plt.legend(frameon=False,ncol=2)
plt.title("Preliminary DC-DC Output Current Monitor Measurement \n AMACv2 #%d, VDD=%.3G V, BG600=%d mV, Ramp=01, Reg 58 = 0x00E01800, \n Overlay of 200 measurements" %(amac_num, VDD, bgmV))

# Plot distributions of a few select points
mean_current = mA_per_count * (np.mean(amac_counts[point1_start:point1_end]) - offset_current) / 1000
std_current = mA_per_count * (np.std(amac_counts[point1_start:point1_end]))
plt.subplot(3,3,7)
plt.hist(amac_counts[point1_start:point1_end], label = "mean = %.3G A" %mean_current + "\nsigma = %.3G mA " %std_current)
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt1 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt1.legendHandles:
	item.set_visible(False)
	
mean_current = mA_per_count * (np.mean(amac_counts[point2_start:point2_end]) - offset_current) / 1000
std_current = mA_per_count * (np.std(amac_counts[point2_start:point2_end]))
plt.subplot(3,3,8)
plt.hist(amac_counts[point2_start:point2_end], label = "mean = %.3G A" %mean_current + "\nsigma = %.3G mA " %std_current)
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt2 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt2.legendHandles:
	item.set_visible(False)

mean_current = mA_per_count * (np.mean(amac_counts[point3_start:point3_end]) - offset_current) / 1000
std_current = mA_per_count * (np.std(amac_counts[point3_start:point3_end]))
plt.subplot(3,3,9)
plt.hist(amac_counts[point3_start:point3_end], label = "mean = %.3G A" %mean_current + "\nsigma = %.3G mA " %std_current)
plt.xlabel('AMAC counts')
plt.ylabel('# entries')
plt3 = plt.legend(frameon=False,ncol=2,fontsize='small',handlelength=0, handletextpad=0)
for item in plt3.legendHandles:
	item.set_visible(False)
	
	
plt.show()
#plt.savefig('OutputCM.pdf')
