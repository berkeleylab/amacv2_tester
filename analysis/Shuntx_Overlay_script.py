import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import csv

v_per_count = 5.0/65535.0	#LT2666 DAC set in 5V range
divider = 3.4/5.0			# ~2/3 divider made from RB15/RB12
dac_steps_in_nA = v_per_count * divider * 1e6

matplotlib.rcParams['font.size'] = 15
matplotlib.rcParams['figure.figsize']=(12,8)
matplotlib.rcParams['legend.fontsize']=20 

filename = ['Shuntx_AMAC05_test.csv', 'Shuntx_AMAC09_test.csv', 'Shuntx_AMAC10_test.csv']

amac_num = [5, 9, 10]				# serial number
VDD = [1.24, 1.255,  1.23]					# in V, determined via calibration
bgmV = [656, 673, 658]					# in mV
ramp = [03, 01, 01]	#AM Int Slope values

# Import data
inputShuntx = []
amac_counts_AMAC05 = []
amac_counts_AMAC09 = []
amac_counts_AMAC10 = []

with open(filename[0], 'rb') as csvfile:	# generated from Shuntx_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		inputShuntx.append(float(column[1]))
		amac_counts_AMAC05.append(float(column[2]))

with open(filename[1], 'rb') as csvfile:	# generated from Shuntx_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		inputShuntx.append(float(column[1]))
		amac_counts_AMAC09.append(float(column[2]))

with open(filename[2], 'rb') as csvfile:	# generated from Shuntx_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		inputShuntx.append(float(column[1]))
		amac_counts_AMAC10.append(float(column[2]))		
		
		
# Find fit of AMACv2 counts v. current line; gives gain 
label_data = ("AMACv2 #%d, VDD=%.3G V, BG600=%d mV, Ramp=%d, DAC bias = 0D, Load = 1 KOhm" %(amac_num[0], VDD[0], bgmV[0], ramp[0]))
plt.plot(inputShuntx[0:255], amac_counts_AMAC05[0:255], '.', color = "blue", label= label_data)



label_data = ("AMACv2 #%d, VDD=%.3G V, BG600=%d mV, Ramp=%d, DAC bias = 0D, Load = 1 KOhm" %(amac_num[1], VDD[1], bgmV[1], ramp[1]))
plt.plot(inputShuntx[0:255], amac_counts_AMAC09[0:255], '.', color = "red", label= label_data)


label_data = ("AMACv2 #%d, VDD=%.3G V, BG600=%d mV, Ramp=%d, DAC bias = 0D, Load = 1 KOhm" %(amac_num[2], VDD[2], bgmV[2], ramp[2]))
plt.plot(inputShuntx[0:255], amac_counts_AMAC10[0:255], '.', color = "green", label= label_data)

#plt.plot(inputShuntx[50:214], fitted_amac_counts[50:214], '-', label="fit line", color = "red")
plt.xlabel(' DAC ShuntX Input(00-FF)')
plt.ylabel('AMAC counts')
plt.legend(frameon=True,ncol=1, loc=4, fontsize=12)
plt.title("Overlay of DAC Shunt X results (Channel 5 read) of AMAC #%d, #%d and #%d" %(amac_num[0], amac_num[1], amac_num[2]))

plt.show()
#plt.savefig('OutputCM.pdf')
