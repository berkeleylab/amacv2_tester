import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import csv

v_per_count = 5.0/65535.0	#LT2666 DAC set in 5V range
divider = 3.4/5.0			# ~2/3 divider made from RB15/RB12
dac_steps_in_nA = v_per_count * divider * 1e6

matplotlib.rcParams['font.size'] = 15
matplotlib.rcParams['figure.figsize']=(12,8)
matplotlib.rcParams['legend.fontsize']=20 

filename = 'Shuntx_test.csv'
filename2 = 'Shuntx_test_noload.csv'
amac_num = 9				# serial number
VDD = 1.255					# in V, determined via calibration
bgmV = 673					# in mV
ramp = 01

# Import data
inputCaly = []
inputCaly_noload = []
amac_counts = []
with open(filename, 'rb') as csvfile:	# generated from Shuntx_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		inputCaly.append(float(column[1]))
		amac_counts.append(float(column[2]))
		
with open(filename2, 'rb') as csvfile:	# generated from Shuntx_test.cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		inputCaly_noload.append(float(column[1]))
				
		
		
# Find fit of AMACv2 counts v. current line; gives gain 
m, b = np.polyfit(inputCaly, amac_counts, 1)
fitted_amac_counts = np.multiply(inputCaly, m) + b

m2, b = np.polyfit(inputCaly_noload, amac_counts, 1)

plt.plot(inputCaly, amac_counts, '.', color = "red", label="1 KOhm load")
plt.plot(inputCaly_noload, amac_counts, '_', color = "blue", label="no load")
#plt.plot(inputCaly[50:214], fitted_amac_counts[50:214], '-', label="fit line", color = "red")
plt.xlabel(' ShuntX Input(00-FF)')
plt.ylabel('AMAC counts')
plt.legend(frameon=True,ncol=1, loc=4, fontsize=12)
plt.title("Preliminary Shuntx DAC Output Measurement \n AMACv2 #%d, VDD=%.3G V, BG600=%d mV, Ramp=%d, DAC bias = 0D" %(amac_num, VDD, bgmV, ramp))

plt.show()
#plt.savefig('OutputCM.pdf')
