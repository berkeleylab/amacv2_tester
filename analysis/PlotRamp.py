import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

import glob
import argparse

def main(args) :
    matplotlib.rcParams['font.size'] = 15
    matplotlib.rcParams['figure.figsize']=(12,8)
    matplotlib.rcParams['legend.fontsize']=10 

    # filepaths to look for
    filepath = args.prefix+"*"+args.suffix

    # temp storage of the pandas dataframes
    listOfData=[]

    # for ease of use
    listOfTimes=[]

    # reference time = 0
    initialTime = None

    for inpath in sorted(glob.glob(filepath)) :
        print inpath

        # read using pandas dataframe
        data=pd.read_csv(inpath,sep='\t')

        # parse string for the unixtime
        unixtime = inpath
        unixtime = unixtime.replace(args.prefix,"")
        unixtime = unixtime.replace(args.suffix,"")
        unixtime = unixtime.replace("_","")

        # set initial time
        if not initialTime :
            initialTime = int(unixtime)

        # subtract off the initial time
        deltaT = int(unixtime) - initialTime
        print deltaT

        # extra variables for time
        data['seconds'] = deltaT
        data['minutes'] = deltaT/60.
        data['hours'] = deltaT/(60.*60.)
        data['days'] = deltaT/(24.*60.*60.)

        listOfTimes.append(deltaT)

        listOfData.append(data)

    # concat into a single pandas dataframe
    data=pd.concat(listOfData)

    # plot things!
    xVars = ["InputVoltage"]
    yVars = ["ADCvalue"] # etc

    # copy the data into a new dataframe and then apply cuts
    data_subset = data.copy()
    data_subset = data_subset[data_subset["AMBG"]==0]
    data_subset = data_subset[data_subset["RampGain"]==0]


    for xVar in xVars :
        for yVar in yVars :

            print "plotting "+yVar+" vs. "+xVar

            # setup colors for us to plot using the "viridis" color map, with
            # 0.1 and 0.9 indicating to only use the middle 80% of this color range
            # (since e.g. the last few percent of the range is too light to easily see)
            colors = iter(matplotlib.cm.viridis(np.linspace(0.1,0.9,len(glob.glob(filepath)))))

            for timeInSeconds in listOfTimes :
                
                data_subset_time = data_subset.copy()
                data_subset_time = data_subset_time[data_subset_time["seconds"] == timeInSeconds]

                plt.plot(data_subset_time[xVar],data_subset_time[yVar],'.',color=next(colors),label="%.0f"%(timeInSeconds/60.)+" minutes")

            xVarWithUnit = formatVariable(xVar)
            yVarWithUnit = formatVariable(yVar)

            strLabel = yVarWithUnit+" vs. "+xVarWithUnit
            plt.xlabel(xVarWithUnit)
            plt.ylabel(yVarWithUnit)
            plt.legend(frameon=True,ncol=2)
            plt.title(strLabel)

            plt.show()
            outfilepath = args.prefix+"_"+yVar+"_vs_"+xVar+".pdf"
            print "writing "+outfilepath
            plt.savefig(outfilepath)
            plt.clf() # clear figure
    
    return


# for putting units onto axis labels
def formatVariable(var) :
    if var == "InputVoltage" :
        return "Input Voltage [V]"
    elif var == "ADCvalue" :
        return "CAL [counts]"

    # catch all
    return var



if __name__ == "__main__" :

    parser = argparse.ArgumentParser()

    parser.add_argument("--prefix",help="Prefix for csv files used as input",type=str,default="",required=False)
    parser.add_argument("--suffix",help="Suffix for csv files used as input",type=str,default="VADC_CAL.csv",required=False)

    args = parser.parse_args()

    # strip away trailing (leading) underscore from prefix (suffix)
    args.prefix = args.prefix.rstrip("_")
    args.suffix = args.suffix.lstrip("_")

    main(args)

