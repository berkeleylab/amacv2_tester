import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

import glob
import argparse

def main(args) :
    matplotlib.rcParams['font.size'] = 15
    matplotlib.rcParams['figure.figsize']=(12,8)
    matplotlib.rcParams['legend.fontsize']=20 

    # filepaths to look for
    filepath = args.prefix+"*"+args.suffix

    # temp storage of the pandas dataframes
    listOfData=[]

    # reference time = 0
    initialTime = None

    for inpath in sorted(glob.glob(filepath)) :
        print inpath

        # read using pandas dataframe
        data=pd.read_csv(inpath,sep='\t')

        # parse string for the unixtime
        unixtime = inpath
        unixtime = unixtime.replace(args.prefix,"")
        unixtime = unixtime.replace(args.suffix,"")
        unixtime = unixtime.replace("_","")

        # set initial time
        if not initialTime :
            initialTime = int(unixtime)

        # subtract off the initial time
        deltaT = int(unixtime) - initialTime
        print deltaT

        # extra variables for time
        data['seconds'] = deltaT
        data['minutes'] = deltaT/60.
        data['hours'] = deltaT/(60.*60.)
        data['days'] = deltaT/(24.*60.*60.)

        # supply current is the sum of VDDLR and VDCDC current
        # FIXME double check that there isn't a minus sign I might be forgetting!!
        data['supply_current'] = data['ADC_AM_VDDLR_A']+data['ADC_AM_VDCDC_A']

        listOfData.append(data)

    # concat into a single pandas dataframe
    data=pd.concat(listOfData)

    # plot things!
    xVars = ["seconds","HVret"]
    yVars = ["HVret","AM900BG","AM600BG","ADC_AM_VDD_V","ADC_AM_VDDLR_A","ADC_AM_VDDLR_V","ADC_Ext_NTC_ADC1","supply_current"] # etc

    for xVar in xVars :
        for yVar in yVars :

            print "plotting "+yVar+" vs. "+xVar

            plt.plot(data[xVar],data[yVar],'.')

            xVarWithUnit = formatVariable(xVar)
            yVarWithUnit = formatVariable(yVar)

            strLabel = yVarWithUnit+" vs. "+xVarWithUnit
            plt.xlabel(xVarWithUnit)
            plt.ylabel(yVarWithUnit)
            plt.title(strLabel)
            plt.show()

            outfilepath = args.prefix+yVar+"_vs_"+xVar+".pdf"
            print "writing "+outfilepath
            plt.savefig(outfilepath)
            plt.clf() # clear figure
    
    return


# for putting units onto axis labels
def formatVariable(var) :
    if var == "seconds" :
        return "time [s]"
    elif var == "minutes" :
        return "time [m]"
    elif var == "hours" :
        return "time [h]"
    elif var == "days" :
        return "time [d]"

    if var == "supply_current" :
        return "supply current [A]"

    if "ADC_" not in var :
        if var == "VDCDC" or var == "VDDLR" :
            var = var+"/2"
        elif var == "VDDREG" :
            var = "2/3 * "+var

        return var+" [counts]"

    else :
        if var.endswith("_A") :
            return var+" [A]"
        else :
            return var+" [V]"

    # catch all, though we'll currently never reach it
    return var



if __name__ == "__main__" :

    parser = argparse.ArgumentParser()

    parser.add_argument("--prefix",help="Prefix for csv files used as input",type=str,default="",required=False)
    parser.add_argument("--suffix",help="Suffix for csv files used as input",type=str,default="ReadAllAMChansAndADCs.csv",required=False)

    args = parser.parse_args()

    # format such that prefix ends with an underscore and suffix starts with one
    if not args.prefix.endswith("_") :
        args.prefix = args.prefix + "_"
    if not args.suffix.startswith("_") :
        args.suffix = "_" + args.suffix

    main(args)

