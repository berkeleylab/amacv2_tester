#--------- This script overlays the Input DC-DC Current Monitor results of three AMACs #5, #9 and #10---------------
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import csv

v_per_count = 5.0/65535.0	#LT2666 DAC set in 5V range
divider = 3.4/5.0			# ~2/3 divider made from RB15/RB12
dac_steps_in_nA = v_per_count * divider * 1e6

matplotlib.rcParams['font.size'] = 15
matplotlib.rcParams['figure.figsize']=(12,8)
matplotlib.rcParams['legend.fontsize']=20 

# Import data
current = []
amac_counts_AMAC05 = []
amac_counts_AMAC09 = []
amac_counts_AMAC10 = []

with open('InputCM_AMAC05_200run.csv', 'rb') as csvfile:	# generated from .cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		current.append(float(column[1]))
		amac_counts_AMAC05.append(float(column[2]))
		
with open('InputCM_AMAC09_200run.csv', 'rb') as csvfile:	# generated from .cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		current.append(float(column[1]))
		amac_counts_AMAC09.append(float(column[2]))
		
with open('InputCM_AMAC10_200run.csv', 'rb') as csvfile:	# generated from .cpp
	reader = csv.reader(csvfile, delimiter="\t")
	header = next(reader)
	for column in reader:
		current.append(float(column[1]))
		amac_counts_AMAC10.append(float(column[2]))
		
#___________Plot AMAC Sl no 5___________________________________________________________________________________________________________________________
m, b = np.polyfit(current[0:10199], amac_counts_AMAC05[0:10199], 1)
fitted_amac_counts_AMAC05 = np.multiply(current[0:10199], m) + b
mA_per_count = 1.0/m * 1e3
fit_data = "(%.3G mA/count), " %mA_per_count + "VDD= 1.24 V, BG600=656 mV, Ramp=03, Reg 58 =0x00401820"

#offset_current = np.mean(amac_counts[0:9])	# mean of the first point (offset)
# Plot overlayed data
#plt.subplot(3,3,(1,6))
plt.plot(current[0:10199], amac_counts_AMAC05[0:10199], '.', color='blue')

plt.plot(current[0:10199], fitted_amac_counts_AMAC05[0:10199], '-', color='blue', label="AMAC Sl. #5 fit: " + fit_data)	

#___________Plot AMAC Sl no 9___________________________________________________________________________________________________________________________
m, b = np.polyfit(current[0:10199], amac_counts_AMAC09[0:10199], 1)
fitted_amac_counts_AMAC09 = np.multiply(current[0:10199], m) + b
mA_per_count = 1.0/m * 1e3
fit_data = "(%.3G mA/count), " %mA_per_count + "VDD= 1.255 V, BG600=673 mV, Ramp=01, Reg 58 =0x00401605"

#offset_current = np.mean(amac_counts[0:9])	# mean of the first point (offset)
# Plot overlayed data
#plt.subplot(3,3,(1,6))
plt.plot(current[0:10199], amac_counts_AMAC09[0:10199], '.', color='red')

plt.plot(current[0:10199], fitted_amac_counts_AMAC09[0:10199], '-', color='red', label="AMAC Sl. #9: " + fit_data)		

#___________Plot AMAC Sl no 10___________________________________________________________________________________________________________________________
m, b = np.polyfit(current[0:10199], amac_counts_AMAC10[0:10199], 1)
fitted_amac_counts_AMAC10 = np.multiply(current[0:10199], m) + b
mA_per_count = 1.0/m * 1e3
fit_data = "(%.3G mA/count), " %mA_per_count + "VDD= 1.23 V, BG600=658 mV, Ramp=01, Reg 58 =0x00401800"

#offset_current = np.mean(amac_counts[0:9])	# mean of the first point (offset)
# Plot overlayed data
#plt.subplot(3,3,(1,6))
plt.plot(current[0:10199], amac_counts_AMAC10[0:10199], '.', color='green')

plt.plot(current[0:10199], fitted_amac_counts_AMAC10[0:10199], '-', color='green', label="AMAC Sl. #10: " + fit_data)			

#___________________ Set Axis for the plot________________________________________________________________________________________________________________
plt.xlabel('Test current (A)')
plt.ylabel('AMAC counts')
plt.legend(frameon=True,ncol=1, loc=4, fontsize=12)
plt.title("Preliminary Output CM Measurement \n Comparing AMACv2 Sl #5, #9 and #10 @ room temp. \n Overlay of 200 measurements")		

	
plt.show()
#plt.savefig('OutputCM.pdf')
