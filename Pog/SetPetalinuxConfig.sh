#!/bin/bash

# Parse arguments
if [ ${#} != 1 ] && [ ${#} != 2 ]; then
    echo "usage: ${0} key [value]"
    exit 1
fi
key=${1}
value=${2}

# Exit if any of these commands fails
set -e
set -x
# Reference to work directory
config=petalinux/project-spec/configs/config

# Check if current value exists, otherwise insert
if ! grep -q ${key} ${config}; then
    echo "# ${key} is not set" >> petalinux/project-spec/configs/config
fi

# get current value
cat ${config}
current=$(grep ${key} ${config})
if [ "x${value}x" == "xx" ]; then # unset value
    sed -i -e "s|${current}|# ${key} is not set|" petalinux/project-spec/configs/config
else # change value
    sed -i -e "s|${current}|${key}=${value}|" petalinux/project-spec/configs/config
fi

