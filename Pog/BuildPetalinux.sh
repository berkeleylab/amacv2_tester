#!/bin/bash

# Parse arguments
if [ ${#} != 1 ]; then
    echo "usage: ${0} name"
    exit 1
fi
name=${1}

# Exit if any of these commands fails
set -e

POGDIR=$(realpath $(dirname "${BASH_SOURCE[0]}"))

# Reference to work directory
workdir=$(pwd)

for i in $(find bin/ -name '*xsa')
do # Loop over all available hardware artifacts
    project=$(basename ${i} .xsa)
    pname=$(echo ${project} | cut -d- -f1)
    pver=$(echo ${project} | cut -d- -f2-)

    # Setup the version and firmware names
    ${POGDIR}/SetPetalinuxConfig.sh CONFIG_SUBSYSTEM_PRODUCT ${pname}
    ${POGDIR}/SetPetalinuxConfig.sh CONFIG_SUBSYSTEM_FW_VERSION ${pver}

    # Compile!
    pushd  ${workdir}/petalinux

    # Setup hardware for project
    petalinux-config --get-hw-description ${workdir}/bin/${project} --silentconfig

    # Build
    petalinux-build

    # Package
    petalinux-package --boot --fpga ${workdir}/bin/${project}/${project}.bit --fsbl images/linux/zynq_fsbl.elf --u-boot -o images/linux/BOOT.bin --force

    # Save results
    target=${workdir}/bin/${project}/${name}
    if [ ! -e ${target} ]; then
	mkdir ${target}
    fi
    cp images/linux/BOOT.bin ${target}
    cp images/linux/image.ub ${target}
    cp images/linux/boot.scr ${target}
    cp images/linux/rootfs.tar.gz ${target}

    # Clean-up
    popd
done
