#include "AMACTB.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <cmath>


//
// Read and print all AM channels + all ADC channels
//
int main(int argc, char *argv[])
{

  std::shared_ptr<AMACTB> TB;
  TB=std::make_shared<AMACTB>();

  AMACTest test("am_readall", TB);

  // Want to set the three pots with different settings to make use of 
  // the different ranges available to us
  TB->POT0.setResistance((float) 100E3); // NTCy
  TB->POT1.setResistance((float) 50E3); // NTCpb
  TB->POT2.setResistance((float) 10E3); // NTCx
  // Set NTCs to -22C to +30C range (switch = 3b010) with CAL bits off
  TB->END.write_reg(57, 0x000a0a0a); 

  // Want these with a 50 mV offset for the DCDC input/output current
  TB->setDAC(TB->Cur10Vp,5);
  TB->setDAC(TB->Cur10Vp_offset,4.95);

  // Turn on the current mirror and set it to a midrange current
  TB->setDAC(TB->VCC1, 1.0); // UB18. Turns on current mirror circuit
  TB->HVret_DAC.DAC->writeUpdateChan(TB->HVret_DAC.chanNbr, 50);

  // Dump all ADCs + AM channels, which we can parse later to see 
  // e.g. current consumption as well as changes to the AM valyes
  sleep(1);
  test.readADCsAndAMChannels("_Default");

  // Repeat with VDCDC disabled to avoid any effects from the leakage current
  TB->setIO(TB->LD_EN_VDCDC,false);
  sleep(1);
  test.readADCsAndAMChannels("_LDENVDCDC_OFF");

  // return LD_EN_VDCDC and HVret/current mirror to initial state
  TB->setIO(TB->LD_EN_VDCDC,true);
  TB->setDAC(TB->HVret_DAC, 0);
  TB->setDAC(TB->VCC1, 0);
  sleep(1);

  // Now do everything else!

  return 0;
}
