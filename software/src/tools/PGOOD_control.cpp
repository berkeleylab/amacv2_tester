#include <iostream>
#include <iomanip>

#include "UIOCom.h"
#include "AMACTB.h"
#include <unistd.h>

int main(int argc, char* argv[]){

  if(argc != 2){
    std::cerr << "Not enough args! Expected: 0 (low) or 1 (high)" << std::endl;
    return 1;
  }

  std::shared_ptr<DeviceCom> uio = std::make_shared<UIOCom>("/dev/uio0", 0x10000);
  AMACTB a (uio, NULL, NULL, NULL, NULL, NULL, NULL);

  int pGood_on = atoi(argv[1]);

  a.setIO(a.PGOOD,pGood_on);

  usleep(1E6);
  return 0;
}
