#include <unistd.h>

#include <memory>
#include <iostream>
#include <iomanip>
#include <vector>

#include "SPICom.h"
#include "LTC2333.h"
#include "ComIOException.h"
#include "AMACTB.h"
#include <time.h>

int main(int argc, char* argv[])
{
	
  AMACTB tb;

  std::cout << "M1 : " << tb.getADC(tb.M1) << std::endl;
  std::cout << "AM_VDD_V: " << tb.getADC(tb.AM_VDD_V) << std::endl;
  std::cout << "AM_VDDLR_A: " << tb.getADC(tb.AM_VDDLR_A) << std::endl;
  std::cout << "AM_VDDLR_V: " << tb.getADC(tb.AM_VDDLR_V) << std::endl;
  std::cout << "AM_VDCDC_V: " << tb.getADC(tb.AM_VDCDC_V) << std::endl;
  std::cout << "AM_VDCDC_A: " << tb.getADC(tb.AM_VDCDC_A) << std::endl;
  std::cout << "AM_VDD_HI_A: " << tb.getADC(tb.AM_VDD_HI_A) << std::endl;
  std::cout << "AM_VDD_HI_V: " << tb.getADC(tb.AM_VDD_HI_V) << std::endl;
  std::cout << "Ext_NTC_ADC1: " << tb.getADC(tb.Ext_NTC_ADC1) << std::endl;
  std::cout << "AM600BG: " << tb.getADC(tb.AM600BG) << std::endl;
  std::cout << "AM900BG: " << tb.getADC(tb.AM900BG) << std::endl;
  std::cout << "Shunty: " << tb.getADC(tb.Shunty) << std::endl;
  std::cout << "Shuntx: " << tb.getADC(tb.Shuntx) << std::endl;
  std::cout << "CALy: " << tb.getADC(tb.CALy) << std::endl;
  std::cout << "CALx: " << tb.getADC(tb.CALx) << std::endl;
  std::cout << "AM_LVDS_CM0: " << tb.getADC(tb.AM_LVDS_CM0) << std::endl;
  std::cout << "AM_LVDS_CM1: " << tb.getADC(tb.AM_LVDS_CM1) << std::endl;
  std::cout << "CP_HVCtrl0: " << tb.getADC(tb.CP_HVCtrl0) << std::endl;
  std::cout << "CP_HVCtrl1: " << tb.getADC(tb.CP_HVCtrl1) << std::endl;
  std::cout << "CP_HVCtrl2: " << tb.getADC(tb.CP_HVCtrl2) << std::endl;
  std::cout << "CP_HVCtrl3: " << tb.getADC(tb.CP_HVCtrl3) << std::endl;
  std::cout << "AVCC_V: " << tb.getADC(tb.AVCC_V) << std::endl;
  std::cout << "AVDD5_V: " << tb.getADC(tb.AVDD5_V) << std::endl;
  std::cout << "VCC5_V: " << tb.getADC(tb.VCC5_V) << std::endl;
  std::cout << "VDD2V5_V: " << tb.getADC(tb.VDD2V5_V) << std::endl;
  std::cout << "AVEE_V: " << tb.getADC(tb.AVEE_V) << std::endl;
  std::cout << "VEE5_V: " << tb.getADC(tb.VEE5_V) << std::endl;
  std::cout << "VDD1V2_V: " << tb.getADC(tb.VDD1V2_V) << std::endl;
  std::cout << "AVCC_A: " << tb.getADC(tb.AVCC_A) << std::endl;
  std::cout << "VCC5_A: " << tb.getADC(tb.VCC5_A) << std::endl;
  std::cout << "HVret1: " << tb.getADC(tb.HVret1) << std::endl;
  std::cout << "HVret2: " << tb.getADC(tb.HVret2) << std::endl;
	
  return 0;
}
