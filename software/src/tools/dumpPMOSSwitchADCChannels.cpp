#include <unistd.h>

#include <memory>
#include <iostream>
#include <iomanip>
#include <vector>

#include "SPICom.h"
#include "LTC2333.h"
#include "ComIOException.h"
#include "AMACTB.h"
#include <time.h>

int main(int argc, char* argv[])
{
	
  AMACTB tb;

  float AM_VDD_V = tb.getADC(tb.AM_VDD_V);
  float AM_VDDLR_A = tb.getADC(tb.AM_VDDLR_A);
  float AM_VDDLR_V = tb.getADC(tb.AM_VDDLR_V);
  float AM_VDCDC_A = tb.getADC(tb.AM_VDCDC_A);
  float AM_VDCDC_V = tb.getADC(tb.AM_VDCDC_V);

  std::cout << "AM_VDD_V: " << AM_VDD_V << std::endl;
  std::cout << "AM_VDDLR_A: " << AM_VDDLR_A << std::endl;
  std::cout << "AM_VDDLR_V: " << AM_VDDLR_V << std::endl;
  std::cout << "AM_VDCDC_V: " << AM_VDCDC_V << std::endl;
  std::cout << "AM_VDCDC_A: " << AM_VDCDC_A << std::endl;
  std::cout << "total current: " << (AM_VDDLR_A + AM_VDCDC_A) << std::endl;
	
  return 0;
}
