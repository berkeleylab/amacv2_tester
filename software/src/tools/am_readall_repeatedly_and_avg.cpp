#include "AMACTB.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <cmath>


// 
// Helper methods to parse the 32-bit register value into
// the 10-bit AMAC channel values
//
unsigned int getRightTen(unsigned int val){
  return (val & 0x000003FF);
}
unsigned int getMidTen(unsigned int val){
  return (val & 0x000FFC00) >> 10;
}
unsigned int getLeftTen(unsigned int val){
  return (val & 0x3FF00000) >> 20;
}

float getAvg(std::vector<int> vec){
  int n = vec.size();
  float sum = 0;

  for(int val : vec){
    sum += val;
  }
  return sum / float(n);
}
float getRMS(std::vector<int> vec){
  int n = vec.size();
  float avg = getAvg(vec);

  float numerator = 0;
  for(int val : vec){
    numerator += ( (avg-val)*(avg-val) );
  }
  return sqrt(numerator / float(n));

}


//
// Read and print all AM channels
//
int main(int argc, char *argv[])
{

  int nIterations = 1;

  if(argc > 1){
    nIterations = std::atoi(argv[1]);
  }

  std::cout << "nIterations to run is: " << nIterations << std::endl;

  std::ofstream outFile;
  outFile.open("am_readall_repeat_"+AMACTime::getTime()+".csv");

  outFile << "datetime\tVDCDC\tVDDLR\tDCDCin\tVDDREG\tsyst_BG_out\tAM900BG\tAM600BG\tCAL\tAMintref\tCALx\tCALy\tShuntx\tShunty\tDCDCadj\tDie_temp\tNTCxp,n\tNTCyp,n\tNTCpbp,n\tHrefx\tHrefy\tidcdcCMOut\tidcdcVlowTP\tidcdcVhighTP\todcdcCMOut\todcdcVlowTP\todcdcVhighTP\tHVret\tPTAT"<< std::endl;

  std::vector<int> vec_ch0,vec_ch1,vec_ch2,vec_ch3a,vec_ch3b,vec_ch3c,vec_ch4a,vec_ch4b,vec_ch4c,vec_ch5a,vec_ch5b,vec_ch5c,vec_ch5d,vec_ch5e,vec_ch6,vec_ch7,vec_ch8,vec_ch9,vec_ch10,vec_ch11,vec_ch12a,vec_ch12b,vec_ch12c,vec_ch13a,vec_ch13b,vec_ch13c,vec_ch14,vec_ch15;

  for(int iteration = 0; iteration < nIterations; ++iteration){

    AMACTB TB;

    unsigned int initialMuxReg = TB.END.read_reg(53);
    std::cout << "read: Register " << std::dec << 53 << " is: 0x" << std::hex << initialMuxReg << std::endl;

    unsigned int nibbleAMIntCalib = (initialMuxReg & 0xFF000000);
    std::cout << "nibble for AM int calib is: 0x" << std::hex << nibbleAMIntCalib << std::endl;

    // Set MUX to read all of the "channel a" values (AM nibble | 0x00000000)
    unsigned int muxValChanA = nibbleAMIntCalib;
    TB.END.write_reg(53,muxValChanA);
    std::cout << "setting MUX to 0x" << std::hex << muxValChanA << std::endl;
    sleep(1); // note that the sleep is needed to give time for the MUX to work!

    unsigned int data10 = TB.END.read_reg(10);
    std::cout << "read: Register " << std::dec << 10 << " is: 0x" << std::hex << data10 << std::endl;
    int ch0 = getRightTen(data10);
    int ch1 = getMidTen(data10);
    int ch2 = getLeftTen(data10);

    unsigned int data11 = TB.END.read_reg(11);
    std::cout << "read: Register " << std::dec << 11 << " is: 0x" << std::hex << data11 << std::endl;
    int ch3a = getRightTen(data11);
    int ch4a = getMidTen(data11);
    int ch5a = getLeftTen(data11);

    unsigned int data12 = TB.END.read_reg(12);
    std::cout << "read: Register " << std::dec << 12 << " is: 0x" << std::hex << data12 << std::endl;
    int ch6 = getRightTen(data12);
    int ch7 = getMidTen(data12);
    int ch8 = getLeftTen(data12);

    unsigned int data13 = TB.END.read_reg(13);
    std::cout << "read: Register " << std::dec << 13 << " is: 0x" << std::hex << data13 << std::endl;
    int ch9 =  getRightTen(data13);
    int ch10 = getMidTen(data13);  
    int ch11 = getLeftTen(data13); 

    unsigned int data14 = TB.END.read_reg(14);
    std::cout << "read: Register " << std::dec << 14 << " is: 0x" << std::hex << data14 << std::endl;
    int ch12a = getRightTen(data14);
    int ch13a = getMidTen(data14);
    int ch14 =  getLeftTen(data14);
   
    unsigned int data15 = TB.END.read_reg(15);
    std::cout << "read: Register " << std::dec << 15 << " is: 0x" << std::hex << data15 << std::endl;
    int ch15 = getRightTen(data15);

    // set the MUX to read all of the "channel b" values
    unsigned int muxValChanB = nibbleAMIntCalib | 0x111110;
    TB.END.write_reg(53,muxValChanB);
    std::cout << "setting MUX to 0x" << std::hex << muxValChanB << std::endl;
    sleep(1);

    data11 = TB.END.read_reg(11);
    std::cout << "read: Register " << std::dec << 11 << " is: 0x" << std::hex << data11 << std::endl;
    int ch3b = getRightTen(data11);
    int ch4b = getMidTen(data11);  
    int ch5b = getLeftTen(data11); 

    data14 = TB.END.read_reg(14);
    std::cout << "read: Register " << std::dec << 14 << " is: 0x" << std::hex << data14 << std::endl;
    int ch12b = getRightTen(data14);
    int ch13b = getMidTen(data14);

    // set the MUX to read all of the "channel c" values
    unsigned int muxValChanC = nibbleAMIntCalib | 0x222220;
    TB.END.write_reg(53,muxValChanC);
    std::cout << "setting MUX to 0x" << std::hex << muxValChanC << std::endl;
    sleep(1);

    data11 = TB.END.read_reg(11);
    std::cout << "read: Register " << std::dec << 11 << " is: 0x" << std::hex << data11 << std::endl;
    int ch3c = getRightTen(data11);
    int ch4c = getMidTen(data11);  
    int ch5c = getLeftTen(data11); 

    data14 = TB.END.read_reg(14);
    std::cout << "read: Register " << std::dec << 14 << " is: 0x" << std::hex << data14 << std::endl;
    int ch12c = getRightTen(data14);
    int ch13c = getMidTen(data14);

    // set the MUX to read all of the "channel d" values
    unsigned int muxValChanD = nibbleAMIntCalib | 0x3000;
    TB.END.write_reg(53,muxValChanD);
    std::cout << "setting MUX to 0x" << std::hex << muxValChanD << std::endl;
    sleep(1);

    data11 = TB.END.read_reg(11);
    std::cout << "read: Register " << std::dec << 11 << " is: 0x" << std::hex << data11 << std::endl;
    int ch5d = getLeftTen(data11);

    // set the MUX to read all of the "channel e" values
    unsigned int muxValChanE = nibbleAMIntCalib | 0x4000;
    TB.END.write_reg(53,muxValChanE);
    std::cout << "setting MUX to 0x" << std::hex << muxValChanE << std::endl;
    sleep(1);

    data11 = TB.END.read_reg(11);
    std::cout << "read: Register " << std::dec << 11 << " is: 0x" << std::hex << data11 << std::endl;
    int ch5e = getLeftTen(data11);

    std::cout << "ch0:   " << std::dec << ch0 << " (VDCDC)" << std::endl;
    std::cout << "ch1:   " << std::dec << ch1 << " (VDDLR)" << std::endl;
    std::cout << "ch2:   " << std::dec << ch2 << " (DCDCin)" << std::endl;
    std::cout << "ch3a:  " << std::dec << ch3a << " (VDDREG)" << std::endl;
    std::cout << "ch3b:  " << std::dec << ch3b << " (output of system bandgap)" << std::endl;
    std::cout << "ch3c:  " << std::dec << ch3c << " (AM900BG)" << std::endl;
    std::cout << "ch4a:  " << std::dec << ch4a << " (AM600BG)" << std::endl;
    std::cout << "ch4b:  " << std::dec << ch4b << " (CAL)" << std::endl;
    std::cout << "ch4c:  " << std::dec << ch4c << " (AM integrator reference)" << std::endl;
    std::cout << "ch5a:  " << std::dec << ch5a << " (CALx)" << std::endl;
    std::cout << "ch5b:  " << std::dec << ch5b << " (CALy)" << std::endl;
    std::cout << "ch5c:  " << std::dec << ch5c << " (Shuntx)" << std::endl;
    std::cout << "ch5d:  " << std::dec << ch5d << " (Shunty)" << std::endl;
    std::cout << "ch5e:  " << std::dec << ch5e << " (DCDCadj, but is wired to ground)" << std::endl;
    std::cout << "ch6:   " << std::dec << ch6 << " (Die temperature)" << std::endl;
    std::cout << "ch7:   " << std::dec << ch7 << " (NTCxp,n)" << std::endl;
    std::cout << "ch8:   " << std::dec << ch8 << " (NTCyp,n)" << std::endl;
    std::cout << "ch9:   " << std::dec << ch9 << " (NTCpbp,n)" << std::endl;
    std::cout << "ch10:  " << std::dec << ch10 << " (Hrefx)" << std::endl;
    std::cout << "ch11:  " << std::dec << ch11 << " (Hrefy)" << std::endl;
    std::cout << "ch12a: " << std::dec << ch12a << " (idcdcCMOut)" << std::endl;
    std::cout << "ch12b: " << std::dec << ch12b << " (idcdcVlowTP)" << std::endl;
    std::cout << "ch12c: " << std::dec << ch12c << " (idcdcVhighTP)" << std::endl;
    std::cout << "ch13a: " << std::dec << ch13a << " (odcdcCMOut)" << std::endl;
    std::cout << "ch13b: " << std::dec << ch13b << " (odcdcVlowTP)" << std::endl;
    std::cout << "ch13c: " << std::dec << ch13c << " (odcdcVhighTP)" << std::endl;
    std::cout << "ch14:  " << std::dec << ch14 << " (HVret)" << std::endl;
    std::cout << "ch15:  " << std::dec << ch15 << " (PTAT)" << std::endl;

    std::cout << std::endl;

    outFile << std::dec;
    outFile << AMACTime::getTime() << "\t";
    outFile << ch0 << "\t";
    outFile << ch1 << "\t";
    outFile << ch2 << "\t";
    outFile << ch3a << "\t";
    outFile << ch3b << "\t";
    outFile << ch3c << "\t";
    outFile << ch4a << "\t";
    outFile << ch4b << "\t";
    outFile << ch4c << "\t";
    outFile << ch5a << "\t";
    outFile << ch5b << "\t";
    outFile << ch5c << "\t";
    outFile << ch5d << "\t";
    outFile << ch5e << "\t";
    outFile << ch6 << "\t";
    outFile << ch7 << "\t";
    outFile << ch8 << "\t";
    outFile << ch9 << "\t";
    outFile << ch10 << "\t";
    outFile << ch11 << "\t";
    outFile << ch12a << "\t";
    outFile << ch12b << "\t";
    outFile << ch12c << "\t";
    outFile << ch13a << "\t";
    outFile << ch13b << "\t";
    outFile << ch13c << "\t";
    outFile << ch14 << "\t";
    outFile << ch15 << std::endl;

    vec_ch0.push_back(ch0);
    vec_ch1.push_back(ch1);
    vec_ch2.push_back(ch2);
    vec_ch3a.push_back(ch3a);
    vec_ch3b.push_back(ch3b);
    vec_ch3c.push_back(ch3c);
    vec_ch4a.push_back(ch4a);
    vec_ch4b.push_back(ch4b);
    vec_ch4c.push_back(ch4c);
    vec_ch5a.push_back(ch5a);
    vec_ch5b.push_back(ch5b);
    vec_ch5c.push_back(ch5c);
    vec_ch5d.push_back(ch5d);
    vec_ch5e.push_back(ch5e);
    vec_ch6.push_back(ch6);
    vec_ch7.push_back(ch7);
    vec_ch8.push_back(ch8);
    vec_ch9.push_back(ch9);
    vec_ch10.push_back(ch10);
    vec_ch11.push_back(ch11);
    vec_ch12a.push_back(ch12a);
    vec_ch12b.push_back(ch12b);
    vec_ch12c.push_back(ch12c);
    vec_ch13a.push_back(ch13a);
    vec_ch13b.push_back(ch13b);
    vec_ch13c.push_back(ch13c);
    vec_ch14.push_back(ch14);
    vec_ch15.push_back(ch15);
      
    TB.END.write_reg(53,initialMuxReg);
    std::cout << "setting MUX back to its initial value " << initialMuxReg << std::endl;
    sleep(1);
  }

  outFile << std::endl;
  outFile << "avg values: " << std::endl;
  outFile << std::fixed << std::setprecision(2);
  outFile << AMACTime::getTime() << "\t";
  outFile << getAvg(vec_ch0)   << "±" << getRMS(vec_ch0)   << "\t";
  outFile << getAvg(vec_ch1)   << "±" << getRMS(vec_ch1)   << "\t";
  outFile << getAvg(vec_ch2)   << "±" << getRMS(vec_ch2)   << "\t";
  outFile << getAvg(vec_ch3a)  << "±" << getRMS(vec_ch3a)  << "\t";
  outFile << getAvg(vec_ch3b)  << "±" << getRMS(vec_ch3b)  << "\t";
  outFile << getAvg(vec_ch3c)  << "±" << getRMS(vec_ch3c)  << "\t";
  outFile << getAvg(vec_ch4a)  << "±" << getRMS(vec_ch4a)  << "\t";
  outFile << getAvg(vec_ch4b)  << "±" << getRMS(vec_ch4b)  << "\t";
  outFile << getAvg(vec_ch4c)  << "±" << getRMS(vec_ch4c)  << "\t";
  outFile << getAvg(vec_ch5a)  << "±" << getRMS(vec_ch5a)  << "\t";
  outFile << getAvg(vec_ch5b)  << "±" << getRMS(vec_ch5b)  << "\t";
  outFile << getAvg(vec_ch5c)  << "±" << getRMS(vec_ch5c)  << "\t";
  outFile << getAvg(vec_ch5d)  << "±" << getRMS(vec_ch5d)  << "\t";
  outFile << getAvg(vec_ch5e)  << "±" << getRMS(vec_ch5e)  << "\t";
  outFile << getAvg(vec_ch6)   << "±" << getRMS(vec_ch6)   << "\t";
  outFile << getAvg(vec_ch7)   << "±" << getRMS(vec_ch7)   << "\t";
  outFile << getAvg(vec_ch8)   << "±" << getRMS(vec_ch8)   << "\t";
  outFile << getAvg(vec_ch9)   << "±" << getRMS(vec_ch9)   << "\t";
  outFile << getAvg(vec_ch10)  << "±" << getRMS(vec_ch10)  << "\t";
  outFile << getAvg(vec_ch11)  << "±" << getRMS(vec_ch11)  << "\t";
  outFile << getAvg(vec_ch12a) << "±" << getRMS(vec_ch12a) << "\t";
  outFile << getAvg(vec_ch12b) << "±" << getRMS(vec_ch12b) << "\t";
  outFile << getAvg(vec_ch12c) << "±" << getRMS(vec_ch12c) << "\t";
  outFile << getAvg(vec_ch13a) << "±" << getRMS(vec_ch13a) << "\t";
  outFile << getAvg(vec_ch13b) << "±" << getRMS(vec_ch13b) << "\t";
  outFile << getAvg(vec_ch13c) << "±" << getRMS(vec_ch13c) << "\t";
  outFile << getAvg(vec_ch14)  << "±" << getRMS(vec_ch14)  << "\t";
  outFile << getAvg(vec_ch15)  << "±" << getRMS(vec_ch15)  << std::endl;

  outFile.close();
  
  return 0;
}
