#include "AMACTB.h"
#include "AMAC.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <fstream>

int main()
{
  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  AMACTest test("AMACREF1",TB);

  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

  // Power on
  TB->powerOn();
  std::cout << "Power ON" << std::endl;
  usleep(1E6); // 1s wait for a clean power up

  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);

  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);

  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // Setting which has given us 1 mV / count
  TB->END.write_reg(52, 0x898B); // VDD = 1.206 for AMAC #10 (value from the spec)
  std::cout << "VDD and AM BG" << std::endl;
  usleep(1E6);

  TB->END.write_reg(56, 0x00000404); // HVret current switches
  uint data;
  data=TB->END.read_reg(56);
  std::cout << 56 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  
  // Read baseline value from channel 14
  data=TB->END.read_reg(14);
  std::cout << 14 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  uint ch14_val = (data & 0x3FF00000) >> 20;
  std::cout << "ch 14 value:" << "\t" << ch14_val << std::dec << std::endl;

  TB->setDAC(TB->VCC1, 1.0); // UB18. Turns on current mirror circuit

  std::ofstream fh;
  fh.open("HVret_ramp_"+AMACTime::getTime()+".csv");
  fh << "Channel\tDAC Counts\tAMAC Counts\tdatetime" << std::endl;
  for(uint16_t counts=0;counts<=110;counts+=1)	// Do DAC setting in counts (need accuracy at finest range setting)
    {
      for(int num_meas=0;num_meas<200;num_meas+=1)
        {
        TB->HVret_DAC.DAC->writeUpdateChan(TB->HVret_DAC.chanNbr, counts);
        std::cout << counts << std::dec << std::endl;
        usleep(10E3);
        data=TB->END.read_reg(14);
        std::cout << 14 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
        uint ch14_val = (data & 0x3FF00000) >> 20;
        std::cout << "ch 14 value:" << "\t" << ch14_val << std::dec << std::endl;
        fh << 14 << "\t" << counts << "\t" << ch14_val << "\t" << AMACTime::getTime() << std::endl;
        }
    }
  fh.close();

  //TB->setDAC(TB->HVret_DAC, 0);
  //TB->setDAC(TB->VCC1, 0);

  return 0;
}
