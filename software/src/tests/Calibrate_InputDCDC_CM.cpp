/* This program is to calibrate the DCDC Input Current Monitor
 * The VDD, AMBG, Ramp Gain values required to get a ramp of 1mV/count are have to be manually for each AMAC in the program.
 * Calibration of the current monitor is manual.
 * Author: V.R.,HEP, UPenn
 * Date: 08/23/2018
 *
 */


#include "AMACTB.h"
#include "AMAC.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <fstream>
#include <EndeavourCom.h>

/* Function to read Input DCDC Cur Mon voltage output on CH 12 a in terms of counts*/
int readOutput(uint32_t reg58Val, std::shared_ptr<AMACTB> TB1, int zeroCh12Val, int reg53Val)
{

  int data,idcdcCMOut;
   
  TB1->END.write_reg(58, reg58Val);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.

  usleep(1E4);
  data=TB1->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  std::cout << "reg 53 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg53Val << std::dec << std::endl;
  // Read baseline value from channel 12 for zero current, which is a 10bit value in Reg 14
  TB1->END.write_reg(53, reg53Val);// Choosing idcdcCM MUX output and AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  
  data=TB1->END.read_reg(14);
  std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
 idcdcCMOut = (data & 0x000003FF);
  
  idcdcCMOut = idcdcCMOut - zeroCh12Val; // Subtract zero Val
  std::cout << "idcdc output value:" << "\t" << idcdcCMOut << std::dec << std::endl;
  return idcdcCMOut;

}

/* Function to read Input DCDC CM Vlow test point on CH 12 b */
int readVLow(uint32_t reg58Val, std::shared_ptr<AMACTB> TB, int zeroCh12Val, int reg53Val)
{
  uint data, vLowVal;
  
  uint32_t reg53Mux = reg53Val & 0xFFF0FFFF;
  reg53Mux = reg53Mux | 0x00010000;
  std::cout << "reg 53 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg53Mux << std::dec << std::endl;

  TB->END.write_reg(53, reg53Mux);// Choosing Vlow MUX output and AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  
  TB->END.write_reg(58, reg58Val);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
  usleep(1E4);
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  data=TB->END.read_reg(14);
  std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  vLowVal = (data & 0x000003FF);
  vLowVal = vLowVal - zeroCh12Val; //Subtract zero pedestal
  std::cout << "VlowTP Value" << "\t" << vLowVal << std::dec << std::endl;
  return vLowVal;

}
/* Function to read Input DCDC CM VHigh test point on CH 12 c */
int readVHigh(uint32_t reg58Val, std::shared_ptr<AMACTB> TB, int zeroCh12Val, int reg53Val)
{
  uint data,vHighVal;
    
  uint32_t reg53Mux = reg53Val & 0xFFF0FFFF;
  reg53Mux = reg53Mux | 0x00020000;
  std::cout << "reg 53 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg53Mux << std::dec << std::endl;

  TB->END.write_reg(53, reg53Mux);// Choosing Vhigh MUX output and AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  
  TB->END.write_reg(58, reg58Val);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
  usleep(1E4);
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  data=TB->END.read_reg(14);
  std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  vHighVal = (data & 0x000003FF);
  vHighVal = vHighVal - zeroCh12Val; //Subtract zero pedestal
  std::cout << "VHigh TP Value" << "\t" << vHighVal << std::dec << std::endl;
  return vHighVal;
  
}

/* Function to calibrate the current mirror stage in the input DCDC CM using idcdcP and idcdcN 3 bit switches */
/* Calibration is done such that VhighTP and vLow TP values are ~ 10 counts apart */

int curMirrorCalib(int reg58Val, int vHighVal, int vLowVal, int reg53Val, std::shared_ptr<AMACTB> TB, int zeroCh12Val)
{

  if((abs(vHighVal - vLowVal) <= 10)) //No need to calibrate if VHigh TP and VlowTP are 10 counts apart
  {
     reg58Val = 0x00401800; //Default
    std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;
    std::cout << "No Current Mirror calibration required"<< std::endl;
    return reg58Val;
  }

  else if(vHighVal > vLowVal)
  {
    
    for(int i=0; i<0x07;i++)
      {
          uint32_t temp = i<<4;
          reg58Val = reg58Val & 0xFFFFFF0F; //Reg 58 has idcdcN calib bits
          reg58Val = reg58Val | temp;

          TB->END.write_reg(58, reg58Val);// Setting values for idcdcOfs,idcdcN and idcdcP.
          usleep(1E4);
          uint32_t data=TB->END.read_reg(58);
          std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
          vLowVal = readVLow(reg58Val,TB, zeroCh12Val, reg53Val); //Read Vlow TP from ch 12
          vHighVal = readVHigh(reg58Val,TB, zeroCh12Val, reg53Val);//Read Vhigh TP from ch 12

         if (abs(vHighVal - vLowVal) <= 10)//if Vhigh - Vlow = |10| then curr mirror is matched. Exit this function
        {
          break;

        }
      }
    
        std::cout << "Current Mirror calibrated using idcdcN."<< std::endl;
        std::cout << "curMirrorCalib(): inside vhigh>vlow : Vlow TP Value:" << "\t" << vLowVal << std::dec << std::endl;
        std::cout << "curMirrorCalib():inside vhigh>vlow: Vhigh TP Value:" << "\t" << vHighVal << std::dec << std::endl;
    
    return reg58Val;
  }
  else if (vLowVal > vHighVal)
  {
    for(int i=0;i<=0x07;i++)
      {
        uint32_t temp = i;

         reg58Val = reg58Val & 0xFFFFFFF0;
         reg58Val = reg58Val | temp;
         std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;
      
         TB->END.write_reg(58, reg58Val);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
         usleep(1E4);
         uint32_t data=TB->END.read_reg(58);
         std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
         vLowVal = readVLow(reg58Val,TB, zeroCh12Val, reg53Val); //Read Vlow TP from ch 12
         std::cout << "curMirrorCalib(): inside vlow>vhigh : Vlow TP Value:" << "\t" << vLowVal << std::dec << std::endl;

         vHighVal = readVHigh(reg58Val,TB, zeroCh12Val, reg53Val);//Read Vhigh TP from ch 12
         std::cout << "curMirrorCalib():inside vlow>vhigh: Vhigh TP Value:" << "\t" << vHighVal << std::dec << std::endl;

         if (abs(vHighVal - vLowVal) <= 10)//if Vhigh - Vlow = |10| then curr mirror is matched. Exit this function
        {
          break;

        }

      }

      std::cout << "Current Mirror calibrated using idcdcP."<< std::endl;
      std::cout << "curMirrorCalib(): inside vlow>vhigh : Vlow TP Value:" << "\t" << vLowVal << std::dec << std::endl;
      std::cout << "curMirrorCalib():inside vlow>vhigh: Vhigh TP Value:" << "\t" << vHighVal << std::dec << std::endl;

    return reg58Val;
  }
  else
  {
      std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;
      std::cout << "Current Mirror calibration not done."<< std::endl;
    return reg58Val;
  }

}

int ampCalib(int idcdcOut, int reg58Val, int reg53Val, std::shared_ptr<AMACTB> TB, int zeroCh12Val, int lowRange, int highRange, int trial, float dacVoltage)
{
  // Four trails are performed to calibrate the current monitor
  // Trial 0: Current mirror calibration value is saved. Only the amplifier bits are varied.
  // Trial 1: Clear current mirror calibration is cleared and amplifier bits are varied.
  // Trial 2: Set only odcdcP10m and vary amplifier bits.
  // Trial 3: Set only odcdcN10m and vary amplifier bits.
  // Note: Trials 1,2,3 adds intential offset to the matched current mirror.

    std::cout << "ampCalib(): DAC voltage/3.121 =" << "\t" << dacVoltage << std::dec << std::endl;
    TB->setDAC(TB->Cur10Vp, dacVoltage); 
    usleep(1E6);
    TB->setDAC(TB->Cur10Vp_offset, dacVoltage);
    usleep(1E6);

    if ((idcdcOut>lowRange) & (idcdcOut<highRange)) // Exit calibration if output is ~ 100 counts
     {
            return reg58Val;
     }

     reg58Val = reg58Val & 0xFFFFF0FF; //Clearing the default offset

    for(int i=0;i<0xF;i++) // Sweeping the 4 bit odcdcOfs switch in reg 58.
        {

          uint32_t temp = i << 8;
          std::cout << "ampCalib():temp value" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << temp << std::dec << std::endl;
           reg58Val = reg58Val & 0xFFFFF0FF;
          reg58Val = reg58Val | temp; 
          TB->END.write_reg(58, reg58Val);
          usleep(1E4);
          int data=TB->END.read_reg(58);
          std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
          usleep(1E4);        

          
          idcdcOut = readOutput(reg58Val, TB, zeroCh12Val, reg53Val); //read Input DCDC CM V output
                    
          if ((idcdcOut>lowRange) & (idcdcOut<highRange))
            {
                break;
            }


        }
	return reg58Val;

}

void currentTest(int reg58Val, int reg53Val, std::shared_ptr<AMACTB> TB, int zeroCh12Val, float dacVoltage)
{
  int data, idcdcOut;


  TB->END.write_reg(53, reg53Val);// Choosing AM Int Slope which gave ~ 1mV/count
  usleep(1E6);
  std::cout << "currentTest(): reg 53 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg53Val << std::dec << std::endl;

  TB->END.write_reg(58, reg58Val);// Setting reg 58 value as determined by the calibration procedure.
  usleep(1E6);
  data=TB->END.read_reg(58);
  std::cout << "currentTest(): reg 58 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  std::cout << "currentTest(): DAC voltage set / 3.121 = " << "\t" << dacVoltage << std::dec << std::endl;
  TB->setDAC(TB->Cur10Vp,dacVoltage); // Set Cur10Vp = dacVoltage * 3.121 V
  usleep(1E6);
  TB->setDAC(TB->Cur10Vp_offset,dacVoltage); // Set Cur10VpOffset = dacVoltage * 3.121 V initially for zero current condition
  usleep(1E6);

  float  dacOffsetVoltage = ((dacVoltage * 3.121) - 0.050)/3.121; //3.121 is the multiplier in the DAC code. Setting Cur10VPOffset 50 mV lower to imitate 1A current through 50 mOhm res.
  std::cout << "currentTest(): Minimum Cur10VpOffset value to be set / 3.121 =" << "\t" << dacOffsetVoltage << std::endl;

  // Read baseline value from channel 12 for zero current, which is a 10bit value in Reg 14

  idcdcOut =  readOutput(reg58Val, TB, zeroCh12Val, reg53Val); //read Input DCDC CM V output. Zero pedestal for ch12 is subtracted from the output in the function.
  std::cout << "currentTest(): Initial idcdcOut value for zero current:" << "\t" << idcdcOut << std::dec << std::endl;
   
  std::ofstream fh;
  fh.open("InputCM_AutoCalibTest_AmacV2.csv");
  fh << "Channel\tCurrent \tInput CM counts\tVLow\tVHigh" << std::endl;


/* Max current measurement requirement for DC-DC Input Current Monitor is 1A, Rsense series resistor is 50 mOhm. (1 A * 50 mOhm = 50 mV). But, DAC program has a multiplication factor as 3.121. So 1.2V * 3.121 = 3.746 @ DAC channel. Setting Cur10Vp =3.746 V and varying Cur10Vp_Offs
et from 3.746 V to 3.696 V to create 50 mV offset in ~ 1 mV count. */

  for(float Cur10VpOffVal= dacVoltage; Cur10VpOffVal>= dacOffsetVoltage;Cur10VpOffVal-=0.00032)
    {
      for(int num_meas=0;num_meas<1;num_meas+=1)
        {
        std::cout << "currentTest(): Cur10VpVal = " << "\t" << Cur10VpOffVal << std::endl;
        TB->setDAC(TB->Cur10Vp, dacVoltage); //Create Vdrop across Rsense resistor. Set Cur10Vp and Vary Cur10VpVal 
        usleep(10E4);
        TB->setDAC(TB->Cur10Vp_offset, Cur10VpOffVal); 
        usleep(10E4); //10m sec delay
      
        int output =  readOutput(reg58Val, TB, zeroCh12Val, reg53Val); //read Input DCDC CM V output
        std::cout << "idcdcOut:" << "\t" << output << std::dec << std::endl;

        
        int vLowVal = readVLow(reg58Val, TB, zeroCh12Val, reg53Val); //Read Vlow TP from ch 13
        std::cout << "main(): Final calibrated VLow value:" << "\t" << vLowVal << std::dec << std::endl;

        int vHighVal = readVHigh(reg58Val, TB, zeroCh12Val, reg53Val);//Read Vhigh TP from ch 13
        std::cout << "main(): Final calibrated Vhigh value:" << "\t" << vHighVal << std::dec << std::endl;
        
        //Writing data to .csv file

        float currIn = (((dacVoltage*3.121) - (Cur10VpOffVal*3.121))/(50*(1E-3))); //Calculating equivalent current

        int currIn10E2 = currIn * 10E2;
        std::cout << "rounded 100:" << "\t" << currIn10E2 << std::dec << std::endl;
        float currInRounded = (float) currIn10E2/10E2; 
        fh <<12 << "\t" << currInRounded << "\t" << output << "\t" << vLowVal << "\t" << vHighVal << std::endl;
        }
    }
  fh.close();

}

int main()
{

  // Modify register values here
  uint32_t reg58Val;
  uint32_t reg53Val = 0x01000000; // For idcdcCMOut.
  uint32_t reg52Val = 0x898c; // Varies for each AMAC #
  int data, zeroCh12Val;
  int idcdcOut, vLowVal, vHighVal;
  
  float dacVoltage = 1.2; //Provide DAC voltage to be set for Cur10Vp and Cur10VpOffset divided by 3.121 here.

  // Range for 100 count offset
  int lowRange = 90;
  int highRange = 100;
  
  int trial =0; //REMOVE

  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  AMACTest test("AMACREF1",TB);

  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

  // Power on
  TB->powerOn();
  std::cout << "Power ON" << std::endl;
  usleep(1E6); // 1s wait for a clean power up

  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);

  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);

  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // FIXME: A second SETID is needed when AMAC reciever comes
  // up in "one" state. Fix when endeavour_master has bit TICKS
  // option for one log clear bit.
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // Setting which has given us 1 mV / count
  TB->END.write_reg(52, reg52Val); // VDD for AMAC #10 (value after ramp calibration)
  usleep(1E4);
  TB->END.write_reg(53, reg53Val);// Choosing AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  std::cout << "main(): reg 52 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg52Val << std::dec << std::endl;
  std::cout << "main(): reg 53 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg53Val << std::dec << std::endl;
  usleep(1E4);
  
  TB->setDAC(TB->Cur10Vp, dacVoltage); // Set Cur10Vp = dacVoltage * 3.121 V
  usleep(1E5);//100 ms delay 
  TB->setDAC(TB->Cur10Vp_offset, dacVoltage);// Set Cur10VpOffset = Cur10Vp for zero current condition.
  usleep(1E5);//100 ms delay 

  //Getting the pedestal - zero value for the output channel 12
  TB->END.write_reg(48, 0x00000101); // Comparator CAL and AM enable set high
  usleep(1E4);
  TB->END.write_reg(49, 0x00000101); // Comparator CAL and AM enable set high
  usleep(1E4);

  data=TB->END.read_reg(14);
  zeroCh12Val = (data & 0x000003FF);
  std::cout << "main(): Zero pedastal for Ch 12 value:" << "\t" << zeroCh12Val << std::dec << std::endl;

  TB->END.write_reg(48, 0x00000001); // Default setting
  usleep(1E4);
  TB->END.write_reg(49, 0x00000001); // Default setting
  usleep(1E4);

  // Input Curr Mon tests

  reg58Val = 0x00401800;
  TB->END.write_reg(58, reg58Val);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
  usleep(1E4);
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

   
  //====================================Step1: ZERO CALIB SWITCH=======================================================
  //To test the mismatch between the buffers on Vlow and VHigh line
  // Set zero calib switch and read Vhigh and Vlow Values
  // if Vhigh - Vlow > 100 counts, discard AMAC
  reg58Val = 0x00481800;
  TB->END.write_reg(58, reg58Val); // VDD for AMAC #10 (value after ramp calibration)
  usleep(1E4);
  std::cout << "main(): Zero Calib reg 58 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  vLowVal = readVLow(reg58Val, TB, zeroCh12Val, reg53Val); //Read Vlow TP from ch 12
  std::cout << "main(): Zero Calib Vlow TP Value:" << "\t" << vLowVal << std::dec << std::endl;

  vHighVal = readVHigh(reg58Val, TB, zeroCh12Val, reg53Val);//Read Vhigh TP from ch 12
  std::cout << "main(): Zero Calib Vhigh TP Value:" << "\t" << vHighVal << std::dec << std::endl;

  if(abs(vHighVal - vLowVal) > 100)
  {
    std::cout << "AMAC not behaving as designed." << std::endl;
    goto jump;

  }

  reg58Val = 0x00401800; // Reset reg58Val
  TB->END.write_reg(58, reg58Val);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
  usleep(1E4);  
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  idcdcOut = readOutput(reg58Val, TB, zeroCh12Val, reg53Val); //Read Input DCDC CM V output
  
  vLowVal = readVLow(reg58Val, TB, zeroCh12Val, reg53Val); //Read Vlow TP from ch 12
  std::cout << "main(): Initial Vlow TP Value:" << "\t" << vLowVal << std::dec << std::endl;

  vHighVal = readVHigh(reg58Val, TB, zeroCh12Val, reg53Val);//Read Vhigh TP from ch 12
  std::cout << "main(): Initial Vhigh TP Value:" << "\t" << vHighVal << std::dec << std::endl;
  
  //=========================================Step2: Curent mirror caliberation ========================================
  reg58Val = curMirrorCalib(reg58Val,vHighVal, vLowVal, reg53Val, TB, zeroCh12Val);// current mirror calibration logic
  TB->END.write_reg(58, reg58Val);// Setting values for idcdcOfs,idcdcN and idcdcP as determined by the zero calibration procedure.
  usleep(1E4);
  data=TB->END.read_reg(58);
  std::cout << "Reg 58 value after calibration" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  //=========================================Step3: Amplifier calibration ===========================================
  
    
  // Calibrating for 100+/- 10 counts initially      
    if((idcdcOut > lowRange) & (idcdcOut < highRange))
    {
      std::cout << "main(): idcdcOut trial 0 calibrated value:" << "\t" << idcdcOut << std::dec << std::endl;
      goto jump;
    }
    
    std::cout << "main(): Calib range: 90-110 counts, Trial No:" << "\t" << trial << std::dec << std::endl;
  
    TB->END.write_reg(53, reg53Val);// Choosing idcdcCM MUX output and AM Int Slope which gave ~ 1mV/count
  usleep(1E4);


    reg58Val = ampCalib(idcdcOut, reg58Val, reg53Val, TB, zeroCh12Val, lowRange, highRange, trial, dacVoltage); // Difference amplifier calibration logic
    TB->END.write_reg(58, reg58Val);// Setting reg 58 value as determined by the calibration procedure.
    usleep(1E6);
    data=TB->END.read_reg(58);
    std::cout <<"main(): Reg 58 val after 90-110 calibration:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;


    //Output should be ~ 100 counts after calibration
    idcdcOut = readOutput(reg58Val, TB, zeroCh12Val, reg53Val); //Read Output DCDC CM V output
    
    if((idcdcOut > lowRange) & (idcdcOut < highRange))
    {
      std::cout << "main(): idcdcOut calibrated value:" << "\t" << idcdcOut << std::dec << std::endl;
      goto jump; //No need to calibrate further
    }

	// Loosening the contraint and calibrating for 100 +/- 20 counts. if answer did not converge on the first run.

    lowRange = 80;
    highRange = 120;
  
    std::cout << "main(): Calib range: 80-120 counts, Trial No:" << "\t" << trial << std::dec << std::endl;

    reg58Val = ampCalib(idcdcOut, reg58Val, reg53Val, TB, zeroCh12Val, lowRange, highRange, trial, dacVoltage); // Difference amplifier calibration logic
    TB->END.write_reg(58, reg58Val);// Setting reg 58 value as determined by the calibration procedure.
    usleep(1E6);
    data=TB->END.read_reg(58);
    std::cout << "main(): Reg 58 val after 80-120 count calibration" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

    TB->END.write_reg(53, reg53Val);// Choosing idcdcCM MUX output and AM Int Slope which gave ~ 1mV/count
    usleep(1E4);
    
    //Output should be ~ 100 counts after calibration
    idcdcOut = readOutput(reg58Val, TB, zeroCh12Val, reg53Val); //Read Output DCDC CM V output
    if((idcdcOut > lowRange) & (idcdcOut < highRange))
    {
      std::cout << "main(): idcdcOut calibrated value:" << "\t" << idcdcOut << std::dec << std::endl;
  
    }

jump:
    vLowVal = readVLow(reg58Val, TB, zeroCh12Val, reg53Val); //Read Vlow TP from ch 13
    std::cout << "main(): Final calibrated VLow value:" << "\t" << vLowVal << std::dec << std::endl;

    vHighVal = readVHigh(reg58Val, TB, zeroCh12Val, reg53Val);//Read Vhigh TP from ch 13
    std::cout << "main(): Final calibrated Vhigh value:" << "\t" << vHighVal << std::dec << std::endl;

    
  //===========================================END CALIBRATION===============================================

  TB->END.write_reg(53, reg53Val);// Choosing idcdcCM MUX output and AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  currentTest(reg58Val, reg53Val, TB, zeroCh12Val, dacVoltage);


  //Resetting DAC channels
  TB->setDAC(TB->Cur10Vp,0); 
  TB->setDAC(TB->Cur10Vp_offset,0); 


  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

  return 0;
}

