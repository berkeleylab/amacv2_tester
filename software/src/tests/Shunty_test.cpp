/* To test the Shuntx DAC output
 * Author: V.R, HEP, UPENN
 * Date: 08/21/2018
 * VDD, BG, AM Int Slope values entered in this program are unique for every AMAC based on ramp calibration.
*/



#include "AMACTB.h"
#include "AMAC.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <fstream>

#include <EndeavourCom.h>

int main()
{
  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  AMACTest test("AMACREF1",TB);

  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

  // Power on
  TB->powerOn();
  std::cout << "Power ON" << std::endl;
  usleep(1E6); // 1s wait for a clean power up

  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);

  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);

  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // Setting which has given us 1 mV / count
  TB->END.write_reg(52, 0x8A8c); // VDD for AMAC #05 (value after ramp calibration)
  usleep(1E4);
  std::cout << "VDD and AM BG values set." << std::endl;
  TB->END.write_reg(53, 0x03003000);// Choosing AM Int Slope which gave ~ 1mV/count and CALx output- MUX 011
  std::cout << "AM Int Slope and Ch5 MUX value set." << std::endl;
  usleep(1E6);

  TB->END.write_reg(55, 0x0000000D);// DAC Bias Setting. Set to default.
  uint data;
  data=TB->END.read_reg(55);
  std::cout << 55 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;


  TB->END.write_reg(54, 0x00FF0000);// Initial DAC input
  data=TB->END.read_reg(54);
  std::cout << 54 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  usleep(1E6);

  //Read initial Channel 5 count.
  data=TB->END.read_reg(11);
  std::cout << 11 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  uint ch5_val = (data & 0x3FF00000) >> 20;
  std::cout << "Ch 5 value:" << "\t" << ch5_val << std::dec << std::endl;

  
  std::ofstream fh;
  fh.open("Shunty_test_"+AMACTime::getTime()+".csv");
  fh << "Channel\tDAC input \t DAC output counts\tdatetime" << std::endl;

  for(int i=0;i<=0xFF;i+=1)	//DAC Shunty inputs are 8 bits in Reg 54
    {
      for(int num_meas=0;num_meas<1;num_meas+=1)
        {

        int shuntYInput = i<<24;
        std::cout << "Shunt y input" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << shuntYInput << std::dec << std::endl;


        TB->END.write_reg(54, shuntYInput);// DAC Shunt X input
        usleep(1E4); //10m sec delay
        data=TB->END.read_reg(54);
        std::cout << 54 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

        data=TB->END.read_reg(11);
        std::cout << 11 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
        uint ch5_val = (data & 0x3FF00000) >> 20;
        std::cout << "ch 5 value:" << "\t" << ch5_val << std::dec << std::endl;
        fh << 5 << "\t" << i << "\t" << ch5_val << "\t" << AMACTime::getTime() << std::endl;
        }
    }
  fh.close();

 
  return 0;
}
