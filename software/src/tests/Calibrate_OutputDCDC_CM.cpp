/* This program is to calibrate the DCDC Output Current Monitor
 * The VDD, AMBG, Ramp Gain manually declared for each AMAC in main().
 * Calibration of the current monitor is automatic. 
 * ~ 100 counts is considered as an initial offset for zero current. 
 * This program first tries to calibrate current monitor to 90-100 counts range.
 * if unsuccessful, the program will calibrate the current monitor to 80-100 counts range.
 * Author: V.R.,HEP, UPenn
 * Date: 08/23/2018
 *
 */


#include "AMACTB.h"
#include "AMAC.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <fstream>

#include <EndeavourCom.h>

/* Function to read Output DCDC Cur Mon voltage output on CH 13 a in terms of counts*/
int readOutput(uint32_t reg58Val, std::shared_ptr<AMACTB> TB1, int zeroCh13Val)
{

   //std::cout << "Inside readOutput()" << std::endl;
//  std::shared_ptr<AMACTB> TB1=std::make_shared<AMACTB>();
  //AMACTest test("AMACREF1",TB1);

   //std::cout << "readOutput(): Pointer initialized" << std::endl;

  int data,odcdcCMOut;
   //std::cout << "readOutput(): intialized data and odcdcCMOut" << std::endl;
  TB1->END.write_reg(58, reg58Val);// Setting values for odcdcOfs,odcdcN and odcdcP as determined by the zero calibration procedure.
//   std::cout << "readOutput(): register 58 written" << std::endl;
  usleep(1E4);
  data=TB1->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  TB1->setDAC(TB1->Cur1Vp, 1.2); // Set Cur1Vp = 3.6/3 = 1.2 V initially to get zero current through "sense resistor".
  TB1->setDAC(TB1->Cur1Vp_offset, 1.2);// Set Cur10VpOffset to 1.1842 to be 50 mV below Cur10Vp.
  usleep(1E6);

  // Read baseline value from channel 12 for zero current, which is a 10bit value in Reg 14
  TB1->END.write_reg(53, 0x03000000);// Choosing odcdcCM MUX output and AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  
  data=TB1->END.read_reg(14);
  std::cout << 13 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  odcdcCMOut = (data & 0x000FFC00) >> 10;
  odcdcCMOut = odcdcCMOut - zeroCh13Val; // Subtract zero Val
  std::cout << "odcdc output value:" << "\t" << odcdcCMOut << std::dec << std::endl;
  return odcdcCMOut;

}

/* Function to read Output DCDC CM Vlow test point on CH 13 b */
int readVLow(uint32_t reg58Val, std::shared_ptr<AMACTB> TB, int zeroCh13Val, int reg53Val)
{
  uint32_t reg53Mux = reg53Val & 0xFF0FFFFF;
  reg53Mux = reg53Mux | 0x00100000;
  std::cout << "reg 53 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg53Mux << std::dec << std::endl;

  uint data, vLowVal;

  //std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  //AMACTest test("AMACREF1",TB);


  TB->END.write_reg(53, reg53Mux);// Choosing Vlow MUX output and AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  
  TB->END.write_reg(58, reg58Val);// Setting values for odcdcOfs,odcdcN and odcdcP as determined by the zero calibration procedure.
  usleep(1E4);
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  data=TB->END.read_reg(14);
  std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  vLowVal = (data & 0x000FFC00) >> 10;
  vLowVal = vLowVal - zeroCh13Val; //Subtract zero pedestal
  std::cout << "VlowTP Value" << "\t" << vLowVal << std::dec << std::endl;
  return vLowVal;

}

/* Function to read Output DCDC CM VHigh test point on CH 13 c */
int readVHigh(uint32_t reg58Val, std::shared_ptr<AMACTB> TB, int zeroCh13Val, int reg53Val)
{
  uint data,vHighVal;
  
//  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  //AMACTest test("AMACREF1",TB);
  
  uint32_t reg53Mux = reg53Val & 0xFF0FFFFF;
  reg53Mux = reg53Mux | 0x00200000;
  std::cout << "reg 53 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg53Mux << std::dec << std::endl;

  TB->END.write_reg(53, reg53Mux);// Choosing Vhigh MUX output and AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  
  TB->END.write_reg(58, reg58Val);// Setting values for odcdcOfs,odcdcN and odcdcP as determined by the zero calibration procedure.
  usleep(1E4);
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  data=TB->END.read_reg(14);
  std::cout << 12 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  vHighVal = (data & 0x000FFC00) >> 10;
  vHighVal = vHighVal - zeroCh13Val; //Subtract zero pedestal
  std::cout << "VHigh TP Value" << "\t" << vHighVal << std::dec << std::endl;
  return vHighVal;
  
}

/* Function to calibrate the current mirror stage in the output DCDC CM using 0dcdcP10m and odcdcN10m 1 bit switches */
int curMirrorCalib(int vHighVal, int vLowVal)
{
 
  int reg58Val;
  
  if((abs(vHighVal - vLowVal) <= 10) | (abs(vLowVal - vHighVal) <= 10)) //No need to calibrate if VHigh TP and VlowTP are 10 counts apart
  {
     reg58Val = 0x00401800; //Default
    std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;
    std::cout << "No Current Mirror calibration required"<< std::endl;
    return reg58Val;
  }
  else if( vHighVal > (vLowVal + 10))
  {
    reg58Val = 0x00411800;
    std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;
    std::cout << "Current Mirror calibrated using odcdcP10m."<< std::endl;
    return reg58Val;
  }
  else if(vLowVal < (vHighVal + 10))
  {
    reg58Val = 0x00421800;
    std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;
    std::cout << "Current Mirror calibrated using odcdcN10m."<< std::endl;
    return reg58Val;
  }
  else
  {
    reg58Val = 0x00401800; //Default
    std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;
    std::cout << "No Current Mirror calibration required"<< std::endl;
    return reg58Val;
  }

}

/* Function to calibarte the difference anmplifier stage of the current mornitor using 4 bit odcdcOfs switches
 *The output will be calibrated to 100 +/- range counts. Edit the code to place stricter rules.
 *
 * */
int ampCalib(int odcdcOut, int reg58Val, std::shared_ptr<AMACTB> TB, int zeroCh13Val, int lowRange, int highRange, int trial)
{
  // Four trails are performed to calibrate the current monitor
  // Trial 0: Current mirror calibration value is saved. Only the amplifier bits are varied.
  // Trial 1: Clear current mirror calibration is cleared and amplifier bits are varied.
  // Trial 2: Set only odcdcP10m and vary amplifier bits.
  // Trial 3: Set only odcdcN10m and vary amplifier bits.
  // Note: Trials 1,2,3 adds intential offset to the matched current mirror.
     
    if ((odcdcOut>lowRange) & (odcdcOut<highRange)) // Exit calibration if output is ~ 100 counts
     {
            return reg58Val;
     }
 
  
    if(trial == 0)
      {
        reg58Val = reg58Val & 0xFF0FFFFF; //Clearing the default -250 mV offset
  
      }
     else if(trial == 1)
     {
        reg58Val = reg58Val & 0xFF00FFFF; //Clearing both odcdcP10m, odcdcN10m and odcdcOfs bits

     }
     else if(trial == 2)
    {
        reg58Val = reg58Val & 0xFF00FFFF;
        reg58Val = reg58Val | 0x00010000; // Setting oP bit
    }
     else if(trial == 3)
    {
        reg58Val = reg58Val & 0xFF00FFFF;
        reg58Val = reg58Val | 0x00020000; // Setting oN bit
    }

    for(int i=0;i<0xF;i++) // Sweeping the 4 bit odcdcOfs switch in reg 58.
        {

          uint32_t temp = i << 20;
          std::cout << "ampCalib():temp value" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << temp << std::dec << std::endl;
           reg58Val = reg58Val & 0xFF0FFFFF;
          reg58Val = reg58Val | temp; 
          TB->END.write_reg(58, reg58Val);
          usleep(1E4);
          int data=TB->END.read_reg(58);
          std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
          usleep(1E4);        

          
           odcdcOut = readOutput(reg58Val, TB, zeroCh13Val); //Read Output DCDC CM V output
                    
          if ((odcdcOut>lowRange) & (odcdcOut<highRange))
            {
                break;
            }


        }
    
    

  return reg58Val;

}

int main()
{

  // Modify register values here
  uint32_t reg58Val;
  uint32_t reg53Val = 0x01000000; // For odcdcCMOut.
  uint32_t reg52Val = 0x898c; // Varies for each AMAC #
  int data, zeroCh13Val;
  int odcdcOut, vLowVal, vHighVal;
  
  // Range for 100 count offset
  int lowRange = 90;
  int highRange = 100;

  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  AMACTest test("AMACREF1",TB);

  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

  // Power on
  TB->powerOn();
  std::cout << "Power ON" << std::endl;
  usleep(1E6); // 1s wait for a clean power up

  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);

  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);

  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // FIXME: A second SETID is needed when AMAC reciever comes
  // up in "one" state. Fix when endeavour_master has bit TICKS
  // option for one log clear bit.
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // Setting which has given us 1 mV / count
  TB->END.write_reg(52, reg52Val); // VDD for AMAC #10 (value after ramp calibration)
  usleep(1E4);
  TB->END.write_reg(53, reg53Val);// Choosing AM Int Slope which gave ~ 1mV/count
  usleep(1E4);
  std::cout << "reg 52 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg53Val << std::dec << std::endl;
  std::cout << "reg 53 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg52Val << std::dec << std::endl;
  usleep(1E4);

  //Getting the pedestal - zero value for the output channel 12
  TB->END.write_reg(48, 0x00000101); // Comparator CAL and AM enable set high
  usleep(1E4);
  TB->END.write_reg(49, 0x00000101); // Comparator CAL and AM enable set high
  usleep(1E4);

  data=TB->END.read_reg(14);
  zeroCh13Val = (data & 0x000FFC00) >> 10;
  std::cout << "Zero pedastal for Ch 13 value:" << "\t" << zeroCh13Val << std::dec << std::endl;

  TB->END.write_reg(48, 0x00000001); // Default setting
  usleep(1E4);
  TB->END.write_reg(49, 0x00000001); // Default setting
  usleep(1E4);


   std::cout << "49 and 49 set to default" << std::endl;
  // Input Curr Mon tests

  //====================================ZERO CALIB SWITCH=======================================================
  //To test the mismatch between the buffers on Vlow and VHigh line
  // Set zero calib switch and read Vhigh and Vlow Values
  // if Vhigh - Vlow > 100 counts, discard AMAC

  TB->END.write_reg(58, 0x00481800); // VDD for AMAC #10 (value after ramp calibration)
  usleep(1E4);
  std::cout << "main(): Zero Calib reg 58 value:" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg52Val << std::dec << std::endl;

  vLowVal = readVLow(reg58Val, TB, zeroCh13Val, reg53Val); //Read Vlow TP from ch 13
  std::cout << "main(): Zero Calib Vlow TP Value:" << "\t" << vLowVal << std::dec << std::endl;

  vHighVal = readVHigh(reg58Val, TB, zeroCh13Val, reg53Val);//Read Vhigh TP from ch 13
  std::cout << "main(): Zero Calib Vhigh TP Value:" << "\t" << vHighVal << std::dec << std::endl;

  if(abs(vHighVal - vLowVal) > 100)
  {
    std::cout << "AMAC not behaving as designed." << std::endl;
    goto jump;

  }

  //===================================END ZERO CALIB SWITCH====================================================
  

  reg58Val = 0x00401800;
  odcdcOut = readOutput(reg58Val, TB, zeroCh13Val); //Read Output DCDC CM V output. Subtract zero pedastal
  std::cout << "main(): Initial ch 13 value:" << "\t" << odcdcOut << std::dec << std::endl;
  

  //====================================START CURRENT MIRROR CALIBRATION ========================================
  
 
  vLowVal = readVLow(reg58Val, TB, zeroCh13Val, reg53Val); //Read Vlow TP from ch 13
  std::cout << "main(): Initial Vlow TP Value:" << "\t" << vLowVal << std::dec << std::endl;

  vHighVal = readVHigh(reg58Val, TB, zeroCh13Val, reg53Val);//Read Vhigh TP from ch 13
  std::cout << "main(): Initial Vhigh TP Value:" << "\t" << vHighVal << std::dec << std::endl;

  reg58Val = curMirrorCalib(vHighVal, vLowVal); //Call current mirror calibration logic
 
  vLowVal = readVLow(reg58Val, TB, zeroCh13Val, reg53Val); //Read Vlow TP from ch 13
  std::cout << "main(): After Cur Mirror calibration, VLow value:" << "\t" << vLowVal << std::dec << std::endl;

  vHighVal = readVHigh(reg58Val, TB, zeroCh13Val, reg53Val);//Read Vhigh TP from ch 13
  std::cout << "main(): After Cur Mirror calibration, Vhigh value:" << "\t" << vHighVal << std::dec << std::endl;

  
  //=====================================END CURRENT MIRROR CALIBRATION===========================================


  odcdcOut = readOutput(reg58Val, TB, zeroCh13Val); //Read Output DCDC CM V output

  //====================================START AMPLIFIER CALIBRATION===============================================
  //4 total trials to calibrate the output current monitor. See funtion ampCalib() for details.

  for(int trial =0; trial < 4; trial++)
  {
    
  //Calibrating for 100+/- 10 counts initially      
    if((odcdcOut > lowRange) & (odcdcOut < highRange))
    {
      std::cout << "main(): odcdcOut trial 0 calibrated value:" << "\t" << odcdcOut << std::dec << std::endl;
      break; //No need to calibrate further
    }
    
    std::cout << "main(): Calib range: 90-110 counts, Trial No:" << "\t" << trial << std::dec << std::endl;

    reg58Val = ampCalib(odcdcOut, reg58Val, TB, zeroCh13Val, lowRange, highRange, trial); // Difference amplifier calibration logic
   // std::cout << "main(): Reg 58 value" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;

    //Output should be ~ 100 counts after calibration
    odcdcOut = readOutput(reg58Val, TB, zeroCh13Val); //Read Output DCDC CM V output
    
    if((odcdcOut > lowRange) & (odcdcOut < highRange))
    {
      std::cout << "main(): odcdcOut calibrated value:" << "\t" << odcdcOut << std::dec << std::endl;
      break; //No need to calibrate further
    }
  }

  // Loosening the contraint and calibrating for 100 +/- 20 counts. if answer did not converge on the first run.

  lowRange = 85;
  highRange = 115;
  for(int trial =0; trial < 4; trial++)
  {
    if((odcdcOut > lowRange) & (odcdcOut < highRange))
    {
      std::cout << "main(): odcdcOut calibrated value:" << "\t" << odcdcOut << std::dec << std::endl;
      break; //No need to calibrate further
    }

    std::cout << "main(): Calib range: 80-120 counts, Trial No:" << "\t" << trial << std::dec << std::endl;

    reg58Val = ampCalib(odcdcOut, reg58Val, TB, zeroCh13Val, lowRange, highRange, trial); // Difference amplifier calibration logic
    std::cout << "main(): Reg 58 value" << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << reg58Val << std::dec << std::endl;

    //Output should be ~ 100 counts after calibration
    odcdcOut = readOutput(reg58Val, TB, zeroCh13Val); //Read Output DCDC CM V output
    if((odcdcOut > lowRange) & (odcdcOut < highRange))
    {
      std::cout << "main(): odcdcOut calibrated value:" << "\t" << odcdcOut << std::dec << std::endl;
      break; //No need to calibrate further
    }
 }

  vLowVal = readVLow(reg58Val, TB, zeroCh13Val, reg53Val); //Read Vlow TP from ch 13
  std::cout << "main(): Final calibrated VLow value:" << "\t" << vLowVal << std::dec << std::endl;

  vHighVal = readVHigh(reg58Val, TB, zeroCh13Val, reg53Val);//Read Vhigh TP from ch 13
  std::cout << "main(): Final calibrated Vhigh value:" << "\t" << vHighVal << std::dec << std::endl;

  //====================================END AMPLIFIER CALIBRATION=================================================
  
  jump:
  
  //Resetting DAC channels
  TB->setDAC(TB->Cur1Vp,0); 
  TB->setDAC(TB->Cur1Vp_offset,0); 


  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

}

