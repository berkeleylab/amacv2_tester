// This tester automatically tests 5 HV I Gain cases 000-1000. The results are available as HVRet1_data_SWXXX.csv in the build folder.
// Switch condition 1000 is tested only upto 3.3 mA. For the full range, change DAC output V divider value.

#include "AMACTB.h"
#include "AMAC.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <fstream>

int main()
{
  //DAC range (5 V*(2/3)/ (2^16))*swCoount. 
  //Last 1000 cannot be tested fully upto 5.5 mA with this voltage div value.
  uint16_t swCount[] = {110, 1100, 11000, 65535, 65535};
  uint16_t swStep[] = {1, 10, 100, 500, 500};
  std::string oFileName;
  
  
  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  AMACTest test("AMACREF1",TB);

  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

  // Power on
  TB->powerOn();
  std::cout << "Power ON" << std::endl;
  usleep(1E6); // 1s wait for a clean power up

  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);

  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);

  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // Setting which has given us 1 mV / count
  TB->END.write_reg(52, 0x898B); // VDD = 1.277 for AMAC #09 (value from the bench test)
  std::cout << "VDD and AM BG" << std::endl;
  usleep(1E6);
  
  for(int i=0;i<=4;i++)
  {
    
     switch(i)
      {

        case 0 :
        
          TB->END.write_reg(56, 0x00000404); //HVret switch 0000 condition
          oFileName = "HVRet1_data_sw0000_"+AMACTime::getTime()+".csv";
          std::cout << "AM HV Cur mon switch 0000 results" << std::endl;
          break;

        case 1 :
    
          TB->END.write_reg(56, 0x00010404); //HVRet switch 0001 condition
          oFileName = "HVRet1_data_sw0001_"+AMACTime::getTime()+".csv";
          std::cout << "AM HV Cur mon switch 0001 results" << std::endl;
          break;

        case 2 :
    
          TB->END.write_reg(56, 0x00020404); //HVRet switch 0010 condition
          oFileName = "HVRet1_data_sw0010_"+AMACTime::getTime()+".csv";
          std::cout << "AM HV Cur mon switch 0010 results" << std::endl;
          break;

        case 3 :
  
          TB->END.write_reg(56, 0x00040404); // HVRet switch 0100 condition
          oFileName = "HVRet1_data_sw0100_"+AMACTime::getTime()+".csv";
          std::cout << "AM HV Cur mon switch 0100 results" << std::endl;
          break;
    
        case 4 :

          TB->END.write_reg(56, 0x00080404); // HVRet switch 1000 condition
          oFileName = "HVRet1_data_sw1000_"+AMACTime::getTime()+".csv";
          std::cout << "AM HV Cur mon switch 1000 results" << std::endl;
          break;
      
      
      }
  

    uint data;
    data=TB->END.read_reg(56);
    std::cout << 56 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  
    // Read baseline value from channel 14
    data=TB->END.read_reg(14);
    std::cout << 14 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
    uint ch14_val = (data & 0x3FF00000) >> 20;
    std::cout << "ch 14 value:" << "\t" << ch14_val << std::dec << std::endl;

    TB->setDAC(TB->VCC1, 1.0); // UB18. Turns on current mirror circuit

    std::ofstream fh;
    fh.open(oFileName);
    fh << "Channel\tDAC Counts\tAMAC Counts\tdatetime" << std::endl;
    for(uint16_t counts=0;counts<=swCount[i];counts+=swStep[i])	// Do DAC setting in counts (need accuracy at finest range setting)
      {
        for(int num_meas=0;num_meas<200;num_meas+=1) // 200 runs to get a good distribution.
          {
           TB->HVret_DAC.DAC->writeUpdateChan(TB->HVret_DAC.chanNbr, counts);
           std::cout << counts << std::dec << std::endl;
           usleep(10E3);
           data=TB->END.read_reg(14);
           std::cout << 14 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
           uint ch14_val = (data & 0x3FF00000) >> 20;
           std::cout << "ch 14 value:" << "\t" << ch14_val << std::dec << std::endl;
           fh << 14 << "\t" << counts << "\t" << ch14_val << "\t" << AMACTime::getTime() << std::endl;
          }
      }

    fh.close();
  }
  //TB->setDAC(TB->HVret_DAC, 0);
  //TB->setDAC(TB->VCC1, 0);

  return 0;
}
