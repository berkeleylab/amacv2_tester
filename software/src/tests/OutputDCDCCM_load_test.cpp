/* This program is written to test the range and slope of the Output DC-DC Current Monitor.
 * The VDD, AM BG and Am Int Slope values required to achieve ~1mV/count ramp have to be manually entered in this program.
 * The Calibration switch  settings also have to be manually entered in the program.
 *
 * Author: V.R., HEP. UPenn
 * Date: 08/16/2018
 *
 */


#include "AMACTB.h"
#include "AMAC.h"
#include "AMACTest.h"
#include "EndeavourComException.h"

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <fstream>

#include <EndeavourCom.h>

int main()
{
  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();
  AMACTest test("AMACREF1",TB);

  // Power off
  TB->powerOff();
  std::cout << "Power OFF" << std::endl;
  usleep(1E6); // 1s wait for a clean power down

  // Power on
  TB->powerOn();
  std::cout << "Power ON" << std::endl;
  usleep(1E6); // 1s wait for a clean power up

  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);

  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);

  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
    usleep(2E6);
  }

  // Setting which has given us 1 mV / count
  TB->END.write_reg(52, 0x898c); // VDD for AMAC #10 (value after ramp calibration)
  TB->END.write_reg(53, 0x01000000);// Choosing AM Int Slope which gave ~ 1mV/count
  std::cout << "VDD and AM BG" << std::endl;
  usleep(1E6);

  TB->END.write_reg(58, 0x00E01800);// Setting values for odcdcOfs,oN and oP as determined by the zero calibration procedure.
  uint data;
  data=TB->END.read_reg(58);
  std::cout << 58 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;

  TB->setDAC(TB->Cur1Vp, 1.2); // Set Cur1Vp = 3.6/3 = 1.2 V initially to get zero current through "sense resistor".
  TB->setDAC(TB->Cur1Vp_offset, 1.2); // Set Cur1Vp = 3.6/3 = 1.2 V
  usleep(1E6);

#if 0
   data=TB->END.read_reg(14);
   std::cout << 14 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
   uint ch13_val = (data & 0x000FFC00) >> 10;
   std::cout << "ch 13 value:" << "\t" << ch13_val << std::dec << std::endl;
#endif
 

   // Read baseline value from channel 13, which is a 10bit value in Reg 14

  data=TB->END.read_reg(14);
  std::cout << 13 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
  uint ch13_val = (data & 0x000FFC00) >> 10;
  std::cout << "ch 13 value:" << "\t" << ch13_val << std::dec << std::endl;

  std::ofstream fh;
  fh.open("OutputCM_test_"+AMACTime::getTime()+".csv");
  fh << "Channel\tCurrent \tOutput CM counts\tdatetime" << std::endl;
  for(float Cur1VpOffVal=1.2;Cur1VpOffVal>=1.150;Cur1VpOffVal-=0.001)	// (4 A / 8 mOhm = 32 mV). Setting offset 50 mV down to measure full range.
    {
      for(int num_meas=0;num_meas<200;num_meas+=1)
        {
           
        TB->setDAC(TB->Cur1Vp, 1.2); //Create Vdrop across Rsense resistor. Set Cur1Vp = 3.6/3 = 1.2 V 
        TB->setDAC(TB->Cur1Vp_offset, Cur1VpOffVal); // Vary Cur1VpOffVal
        usleep(10E5); //1 sec delay
        data=TB->END.read_reg(14);
        std::cout << 14 << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
        uint ch13_val = (data & 0x000FFC00) >> 10;
        std::cout << "ch 13 value:" << "\t" << ch13_val << std::dec << std::endl;

        float currIn = ((1.2 - Cur1VpOffVal)/(8*(1E-3))); //Calculating equivalent current

        int currIn10E2 = currIn * 10E2;
        std::cout << "rounded 100:" << "\t" << currIn10E2 << std::dec << std::endl;
        float currInRounded = (float) currIn10E2/10E2; 
        fh << 13 << "\t" << currInRounded << "\t" << ch13_val << "\t" << AMACTime::getTime() << std::endl;
        }
    }
  fh.close();

  //Resetting DAC channels
  TB->setDAC(TB->Cur1Vp,0); 
  TB->setDAC(TB->Cur1Vp_offset,0); 

  return 0;
}
