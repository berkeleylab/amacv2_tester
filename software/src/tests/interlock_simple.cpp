#include "AMACTB.h"
#include "EndeavourComException.h"
#include "SPICom.h"
#include "LTC2333.h"
#include "ComIOException.h"
#include "UIOCom.h"

#include <time.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>

int main()
{
  std::shared_ptr<AMACTB> TB=std::make_shared<AMACTB>();

  // Power off
  TB->powerOff();
  usleep(1E6);
  std::cout << "Power OFF" << std::endl;
  
  // Power on
  TB->powerOn();
  usleep(1E6); // 1s wait for a clean power up
  std::cout << "Power ON" << std::endl;
  
  // Configure I/O pads
  TB->setIDPads(0x1F);
  TB->setIO(TB->ResetB, true);
  
  // Power on AMAC
  TB->powerAMACOn();
  std::cout << "AMAC ON" << std::endl;
  usleep(1E6);
  
  // Set ID
  try
  {
    TB->END.setid(EndeavourCom::REFMODE::IDPads, 0x1F);
    std::cout << "SETID" << std::endl;
    usleep(1E6);
  }
  catch(EndeavourComException e)
  {
    std::cout << e.what() << std::endl;
  }
  
  // Calibrate AMAC to 1 mV / count
  TB->END.write_reg(52, 0x8a8c); 		// AMAC #5
  std::cout << "Setting VDD and AMBG" << std::endl;
  usleep(1E6);
  TB->END.write_reg(53, 0x03000000);		// AMAC #5
  std::cout << "Setting ramp gain" << std::endl;
  usleep(1E6);

  /*
   * START
   */
  unsigned int val = 0;
  
  TB->setDAC(TB->CAL, 0);			// Reset DAC
  usleep(1E5);
  std::cout << "set: 0"  << std::endl;
  
  TB->END.write_reg(40, 0x66660100); 
  usleep(1E6);
  TB->END.write_reg(41, 0x66660100); 		// Enable LDOx0
  usleep(1E6);
 
  TB->END.write_reg(53, 0x00000100);		// ch3 B (CAL from EoS) selected; 
  usleep(1E6);
  TB->END.write_reg(90, 0x00100000);		// Mask on Hybrid X ch4 hi thresh flag
  usleep(1E6);

  TB->END.write_reg(44, 0x00000001); 		
  usleep(1E6);
  TB->END.write_reg(45, 0x00000001); 		// Master Hybrid X interlock enable
  usleep(1E6);

  TB->END.write_reg(107, (0x200 << 10)); 	// ch4 hi thresh to ~half scale
  usleep(1E6);

  std::cout << std::endl;
  std::cout << "Initial status ..." << std::endl;
  val = TB->END.read_reg(0);
  std::cout << "Status reg is:   0x" << std::hex << val << std::endl;
  val = TB->END.read_reg(1);
  std::cout << "Hx hi flags are: 0x" << std::hex << ((val & 0xFFFF0000) >> 16) << std::endl;
  val = TB->END.read_reg(11);
  std::cout << "ch4 value  is:   " << std::dec << ((val & 0x000FFBFF) >> 10) << std::endl;
  std::cout << "LDx0en: " << TB->readIO(TB->LDx0en) << std::endl << std::endl;

  float voltage = 0;
  while (TB->readIO(TB->LDx0en) == 1){ 
  for (voltage = 0.45; voltage <= 0.55; voltage += 0.01){
      TB->setDAC(TB->CAL, voltage);
      usleep(1E5);
      std::cout << "set: " << std::dec << voltage << std::endl;
    }
  }

  std::cout << "After CAL exceeds threshold ..." << std::endl;
  std::cout << "LDx0en: " << TB->readIO(TB->LDx0en) << std::endl;
  std::cout << "CAL DAC voltage: " << std::dec << voltage << std::endl;
  val = TB->END.read_reg(0);
  std::cout << "Status reg is:   0x" << std::hex << val << std::endl;
  val = TB->END.read_reg(1);
  std::cout << "Hx hi flags are: 0x" << std::hex << ((val & 0xFFFF0000) >> 16) << std::endl;
  val = TB->END.read_reg(11);
  std::cout << "ch4 value  is:   " << std::dec << ((val & 0x000FFC00) >> 10) << std::endl << std::endl;

  TB->END.write_reg(40, 0x66660000); 
  usleep(1E6);
  TB->END.write_reg(41, 0x66660000); 		// Disable LDOx0
  usleep(1E6);

  TB->END.write_reg(90, 0x00000000);		// Mask OFF Hybrid X ch4 hi thresh flag
  usleep(1E6);
  
  std::cout << "After LDOx0 disabled, iLock flag mask off ..." << std::endl;
  val = TB->END.read_reg(0);
  std::cout << "Status reg is:   0x" << std::hex << val << std::endl;
  val = TB->END.read_reg(1);
  std::cout << "Hx hi flags are: 0x" << std::hex << ((val & 0xFFFF0000) >> 16) << std::endl;
  val = TB->END.read_reg(11);
  std::cout << "ch4 value  is:   " << std::dec << ((val & 0x000FFBFF) >> 10) << std::endl << std::endl;
  
  TB->END.write_reg(40, 0x66660100); 
  usleep(1E6);
  TB->END.write_reg(41, 0x66660100); 		// Enable LDOx0 again
  usleep(1E6);
  
  std::cout << "After LDOx0 re-enabled ..." << std::endl;
  std::cout << "LDx0en: " << TB->readIO(TB->LDx0en) << std::endl;
  val = TB->END.read_reg(0);
  std::cout << "Status reg is:   0x" << std::hex << val << std::endl;
  val = TB->END.read_reg(1);
  std::cout << "Hx hi flags are: 0x" << std::hex << ((val & 0xFFFF0000) >> 16) << std::endl;
  val = TB->END.read_reg(11);
  std::cout << "ch4 value  is:   " << std::dec << ((val & 0x000FFBFF) >> 10) << std::endl << std::endl;
 
  TB->setDAC(TB->CAL, 0);			// Reset DAC
  usleep(1E6);
  std::cout << "set: 0" << std::endl;

  std::cout << "After over-limit condition cleared ..." << std::endl;
  std::cout << "LDx0en: " << TB->readIO(TB->LDx0en) << std::endl;
  val = TB->END.read_reg(0);
  std::cout << "Status reg is:   0x" << std::hex << val << std::endl;
  val = TB->END.read_reg(1);
  std::cout << "Hx hi flags are: 0x" << std::hex << ((val & 0xFFFF0000) >> 16) << std::endl;
  val = TB->END.read_reg(11);
  std::cout << "ch4 value  is:   " << std::dec << ((val & 0x000FFBFF) >> 10) << std::endl << std::endl;
  
  TB->END.write_reg(32, 0x1);			// Clear X LDO flags

  std::cout << "After flags reset ..." << std::endl;
  val = TB->END.read_reg(0);
  std::cout << "Status reg is:   0x" << std::hex << val << std::endl;
  val = TB->END.read_reg(1);
  std::cout << "Hx hi flags are: 0x" << std::hex << ((val & 0xFFFF0000) >> 16) << std::endl;
  val = TB->END.read_reg(11);
  std::cout << "ch4 value  is:   " << std::dec << ((val & 0x000FFBFF) >> 10) << std::endl << std::endl;
  
  return 0;
}
