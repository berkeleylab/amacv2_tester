#include "AMACTest.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <ctime>

#include <unistd.h>
#include <math.h>

#include "EndeavourComException.h"

namespace AMACTime{
  std::string getTime(){
    std::time_t theTime = std::time(0);
    char strTime[32];
    std::strftime(strTime, sizeof(strTime), "%Y-%m-%d-%H:%M:%S", std::localtime(&theTime));
    return strTime;
  }
}

AMACTest::AMACTest(const std::string& name, std::shared_ptr<AMACTB> amactb)
  : m_name(name), m_amactb(amactb)
{ }

AMACTest::~AMACTest()
{ }

void AMACTest::dumpRegisters()
{
  uint data;
  for(uint reg=0;reg<172;reg++)
    {
      data=(reg==0)?m_amactb->END.read_reg(reg):m_amactb->END.readnext_reg();
      std::cout << reg << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << data << std::dec << std::endl;
    }
}

void AMACTest::runPower()
{
  std::ofstream fh;
  fh.open(m_name+"_Power_"+AMACTime::getTime()+".csv");

  fh << "AM_VDDLR\tAM_VDDLR_Curr\tAM_VDCDC\tAM_VDCDC_Curr\tAM_VDD_HI\tAM_VDD_HI_Curr\tAM_VDD\tAVCC\tAVCC_Curr\tAVCC5\tAVCC5_Curr\tAVDD5\tVDD2V5\tAVEE\tVEE5\tVDD1V2\tdatetime" << std::endl;

  float AM_VDDLR      =m_amactb->getADC(m_amactb->AM_VDDLR_V);
  float AM_VDDLR_Curr =m_amactb->getADC(m_amactb->AM_VDDLR_A);

  float AM_VDCDC      =m_amactb->getADC(m_amactb->AM_VDCDC_V);
  float AM_VDCDC_Curr =m_amactb->getADC(m_amactb->AM_VDCDC_A);
  
  float AM_VDD_HI     =m_amactb->getADC(m_amactb->AM_VDD_HI_V);
  float AM_VDD_HI_Curr=m_amactb->getADC(m_amactb->AM_VDD_HI_A);

  float AM_VDD        =m_amactb->getADC(m_amactb->AM_VDD_V);

  float AVCC     =m_amactb->getADC(m_amactb->AVCC_V);
  float AVCC_Curr=m_amactb->getADC(m_amactb->AVCC_A);
  float VCC5     =m_amactb->getADC(m_amactb->VCC5_V);
  float VCC5_Curr=m_amactb->getADC(m_amactb->VCC5_A);
  float AVDD5    =m_amactb->getADC(m_amactb->AVDD5_V);
  float VDD2V5   =m_amactb->getADC(m_amactb->VDD2V5_V);
  float AVEE     =m_amactb->getADC(m_amactb->AVEE_V);
  float VEE5     =m_amactb->getADC(m_amactb->VEE5_V);
  float VDD1V2   =m_amactb->getADC(m_amactb->VDD1V2_V);

  fh << AM_VDDLR << "\t" << AM_VDDLR_Curr << "\t" << AM_VDCDC << "\t" << AM_VDCDC_Curr << "\t" << AM_VDD_HI << "\t" << AM_VDD_HI_Curr << "\t" << AM_VDD << "\t" << AVCC << "\t" << AVCC_Curr << "\t" << VCC5 << "\t" << VCC5_Curr << "\t" << AVDD5 << "\t" << VDD2V5 << "\t" << AVEE << "\t" << VEE5 << "\t" << VDD1V2 << "\t" << AMACTime::getTime() << std::endl;

  std::cout << std::fixed << std::setprecision(10) << "AM_VDDLR voltage: "  << AM_VDDLR       << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) << "AM_VDDLR current: "  << AM_VDDLR_Curr  << "A" << std::endl;
  std::cout << std::endl;
  std::cout << std::fixed << std::setprecision(10) << "AM_VDCDC voltage: "  << AM_VDCDC       << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) << "AM_VDCDC current: "  << AM_VDCDC_Curr  << "A" << std::endl;
  std::cout << std::endl;
  std::cout << std::fixed << std::setprecision(10) << "AM_VDD_HI voltage: " << AM_VDD_HI      << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) << "AM_VDD_HI current: " << AM_VDD_HI_Curr << "A" << std::endl;
  std::cout << std::endl;
  std::cout << std::fixed << std::setprecision(10) << "AM_VDD voltage: "    << AM_VDD         << "V" << std::endl;
  std::cout << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"AVCC voltage: "       << AVCC           << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"AVCC current: "       << AVCC_Curr      << "A" << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"VCC5 voltage: "       << VCC5           << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"VCC5 current: "       << VCC5_Curr      << "A" << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"AVDD5 voltage: "      << AVDD5          << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"VDD2V5 voltage: "     << VDD2V5         << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"AVEE voltage: "       << AVEE           << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"VEE5 voltage: "       << VEE5           << "V" << std::endl;
  std::cout << std::fixed << std::setprecision(10) <<"VDD1V2 voltage: "     << VDD1V2         << "V" << std::endl;

  fh.close();

}

void AMACTest::runBERvsClock()
{
  std::ofstream fh;
  fh.open(m_name+"_ENDEAVOUR_CLOCK_"+AMACTime::getTime()+".csv");

  fh << "FreqSetting\tReliability\tdatetime" << std::endl;

  for(uint freqset=0;freqset<pow(2,3);freqset++)
    {
      std::cout << "Setting frequency setting to " << freqset << std::endl;
      m_amactb->END.wrField(&AMACv2Reg::RingOscFrq, freqset);
      float result=runBER(1000);
      fh << freqset << "\t" << result << "\t" << AMACTime::getTime() << std::endl;
    }

  fh.close();
}

void AMACTest::runBERvsCounts()
{
  std::ofstream fh;
  fh.open(m_name+"_ENDEAVOUR_COUNTS_"+AMACTime::getTime()+".csv");

  fh << "FreqSetting\tDIT_MID\tDAH_MID\tReliability\tdatetime" << std::endl;
  
  for(uint freqset=0;freqset<pow(2,3);freqset++)
    {
      m_amactb->END.wrField(&AMACv2Reg::RingOscFrq, freqset);
  
      for(uint DIT_MID=10;DIT_MID<=80;DIT_MID++)
	{
	  m_amactb->END.raw()->setDitMid(DIT_MID);
	  for(uint DAH_MID=190;DAH_MID<=190;DAH_MID++)
	    {
	      m_amactb->END.raw()->setDahMid(DAH_MID);
	      float result=runBER(100);
	      fh << freqset << "\t" << DIT_MID << "\t" << DAH_MID << "\t" << result << "\t" << AMACTime::getTime()<< std::endl;
	    }
	}

      for(uint DIT_MID=35;DIT_MID<=35;DIT_MID++)
	{
	  m_amactb->END.raw()->setDitMid(DIT_MID);
	  for(uint DAH_MID=60;DAH_MID<=400;DAH_MID++)
	    {
	      m_amactb->END.raw()->setDahMid(DAH_MID);
	      float result=runBER(100);
	      fh << freqset << "\t" << DIT_MID << "\t" << DAH_MID << "\t" << result << "\t" << AMACTime::getTime() << std::endl;
	    }
	}

      // Put into usable state
      m_amactb->END.raw()->setDitMid( 14*100./40.);
      m_amactb->END.raw()->setDahMid( 76*100./40.);
    }
  
  fh.close();
}

float AMACTest::runBER(uint trails)
{
  uint good=0;
  for(uint i=0; i<trails; i++)
    {
      try
	{      
	  uint valin=rand()*0xFFFFFFFF;
	  m_amactb->END.write_reg(166,valin);
	  usleep(50);
	  uint valout=m_amactb->END.read_reg(166);
	  usleep(50);
	  if(valin==valout)
	    {
	      good++;
	    }
	  else
	    {
	      std::cout << "Write: 0x" << std::hex << valin << ", Read: " << valout << std::dec << std::endl;
	    }
	}
      catch(EndeavourComException &e)
	{
	  //std::cout << e.what() << std::endl;
	}
    }

  std::cout << "Reliability: " << ((float)good)/trails << std::endl;

  return ((float)good)/trails;
}

void AMACTest::runBandgapScan()
{
  // Enable setting the bandgap
  m_amactb->END.wrField(&AMACv2Reg::AMbgen , 1);
  m_amactb->END.wrField(&AMACv2Reg::VDDbgen, 1);

  std::ofstream fh;
  fh.open(m_name+"_BANDGAP_"+AMACTime::getTime()+".csv");  

  fh << "VDDLR" << "\t" << "VDDBG" << "\t" << "AMBG" << "\t" << "AM600BG" << "\t" << "AM900BG" << "\t" << "VDD" << "\t" << "datetime" << std::endl;

  float VDDLR=m_amactb->getADC(m_amactb->AM_VDDLR_V);;

  for(uint AMbg=0;AMbg<pow(2,4);AMbg++)
    {
      m_amactb->END.wrField(&AMACv2Reg::AMbg, AMbg);
      for(uint VDDbg=0;VDDbg<pow(2,4);VDDbg++)
	{
	  m_amactb->END.wrField(&AMACv2Reg::VDDbg, VDDbg);
	  usleep(10);
	  float AM600BG=m_amactb->getADC(m_amactb->AM600BG);
	  float AM900BG=m_amactb->getADC(m_amactb->AM900BG);
	  float VDD    =m_amactb->getADC(m_amactb->AM_VDD_V);
	  fh << VDDLR << "\t" << VDDbg << "\t" << AMbg << "\t" << AM600BG << "\t" << AM900BG << "\t" << VDD << "\t" << AMACTime::getTime() << std::endl;
	}
    }

  fh.close();
}

void AMACTest::runVoltageADC(const std::string& chname, AMACv2Field AMACv2Reg::* ch, dac_t dac)
{
  std::ofstream fh;
  fh.open(m_name+"_VADC_"+chname+"_"+AMACTime::getTime()+".csv");

  fh << "Channel\tAMBG\tRampGain\tInputVoltage\tADCvalue\tdatetime" << std::endl;

  m_amactb->END.wrField(&AMACv2Reg::AMbgen , 1);
  m_amactb->END.wrField(&AMACv2Reg::AMen,1);
  m_amactb->END.wrField(&AMACv2Reg::AMenC,1);
  usleep(5e3);

  float v_actual;
  for(uint AMbg=0;AMbg<pow(2,4);AMbg++)
    {
      m_amactb->END.wrField(&AMACv2Reg::AMbg, AMbg);
      for(float v=0;v<=1.01;v+=0.01)
	{
	  v_actual=m_amactb->setDAC(dac, v);
	  for(int ramp=0; ramp<16; ramp++)
	    {
	      m_amactb->END.wrField(&AMACv2Reg::AMintCalib, ramp);
	      usleep(10e3);
	      uint32_t counts=m_amactb->END.rdField(ch);
	      fh << chname << "\t" << AMbg << "\t" << ramp << "\t" << v_actual << "\t" << counts << "\t" << AMACTime::getTime() << std::endl;
	    }
	}
    }

  fh.close();
}

void AMACTest::runZeroCalib(const std::string& chname, AMACv2Field AMACv2Reg::* ch)
{
  std::ofstream fh;
  fh.open(m_name+"_ZEROCALIB_"+chname+"_"+AMACTime::getTime()+".csv");

  fh << "Channel\tRampGain\tCounts\tdatetime" << std::endl;

  
  m_amactb->END.wrField(&AMACv2Reg::AMen,1);
  m_amactb->END.wrField(&AMACv2Reg::AMenC,1);
  m_amactb->END.wrField(&AMACv2Reg::AMzeroCalib,1);
  m_amactb->END.wrField(&AMACv2Reg::AMzeroCalibC,1);
  usleep(5e3);
  for(int ramp=0; ramp<16; ramp++)
    {
      m_amactb->END.wrField(&AMACv2Reg::AMintCalib, ramp);
      for(uint i=0;i<100;i++)
	{
	  usleep(5e3);
	  uint32_t counts=m_amactb->END.rdField(ch);
	  fh << chname << "\t" << ramp << "\t" << counts << "\t" << AMACTime::getTime() << std::endl;
	}
    }

  fh.close();
}

void AMACTest::runClocks()
{
  //
  // Create output file
  std::ofstream fh;
  fh.open(m_name+"_CLOCKS_"+AMACTime::getTime()+".csv");

  fh << "FreqSetting" << "\t" << "HVOscFreqSetting" 
     << "\t" << "RingOscFreq"
     << "\t" << "HVOsc0Freq"
     << "\t" << "HVOsc1Freq"
     << "\t" << "HVOsc2Freq"
     << "\t" << "HVOsc3Freq"
     << "\t" << "datetime"
     << std::endl;

  //
  // Enable everything

  // Power HV block
  m_amactb->setDAC(m_amactb->VDDHI_ADJ, 3.3);

  // Enable HV clocks
  m_amactb->END.wrField(&AMACv2Reg::CntSetHV0en,1);	
  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV0en,1);
  m_amactb->END.wrField(&AMACv2Reg::CntSetHV1en,1);	
  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV1en,1);
  m_amactb->END.wrField(&AMACv2Reg::CntSetHV2en,1);	
  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV2en,1);
  m_amactb->END.wrField(&AMACv2Reg::CntSetHV3en,1);	
  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV3en,1);

  // Configure comparator levels
  m_amactb->setDAC(m_amactb->RgOsc_Vref, 0.1);
  m_amactb->setDAC(m_amactb->HVOsc_Vref, 0.15);
  m_amactb->setDAC(m_amactb->HVOsc_Vref, 0.15);
  m_amactb->setDAC(m_amactb->HVOsc_Vref, 0.15);
  m_amactb->setDAC(m_amactb->HVOsc_Vref, 0.15);

  //
  // Configure and start the FreqMeas tests
  std::vector<std::string> clocks={"CLKOUT","HVOSC0","HVOSC1","HVOSC2","HVOSC3"};
  
  for(const std::string& clock : clocks)
    {
      m_amactb->FRQ.set_ts_cnt(clock, 0xFFFFF);
      m_amactb->FRQ.reset(clock);
      m_amactb->FRQ.start(clock);
    }

  //
  // Readout the results
  for(uint freqset=0;freqset<pow(2,3);freqset++)
    {
      m_amactb->END.wrField(&AMACv2Reg::RingOscFrq, freqset);

      for(uint hvfreqset=0;hvfreqset<4;hvfreqset++)
	{
	  m_amactb->END.wrField(&AMACv2Reg::CntSetHV0frq ,hvfreqset);
	  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV0frq,hvfreqset);
	  m_amactb->END.wrField(&AMACv2Reg::CntSetHV1frq ,hvfreqset);
	  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV1frq,hvfreqset);
	  m_amactb->END.wrField(&AMACv2Reg::CntSetHV2frq ,hvfreqset);
	  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV2frq,hvfreqset);
	  m_amactb->END.wrField(&AMACv2Reg::CntSetHV3frq ,hvfreqset);
	  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV3frq,hvfreqset);

	  fh << freqset << "\t" << hvfreqset;
	  usleep(200e3); // Wait for counter to update

	  for(const std::string& clock : clocks)
	    m_amactb->FRQ.read(clock);

	  fh << "\t" << m_amactb->FRQ.get_frq("CLKOUT")
	     << "\t" << m_amactb->FRQ.get_frq("HVOSC0")
	     << "\t" << m_amactb->FRQ.get_frq("HVOSC1")
	     << "\t" << m_amactb->FRQ.get_frq("HVOSC2")
	     << "\t" << m_amactb->FRQ.get_frq("HVOSC3")
             << "\t" << AMACTime::getTime()
	     << std::endl;
	}
    }
  
  //
  // Disable everything

  // Disable HV clocks
  m_amactb->END.wrField(&AMACv2Reg::CntSetHV0en,0);	
  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV0en,0);
  m_amactb->END.wrField(&AMACv2Reg::CntSetHV1en,0);	
  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV1en,0);
  m_amactb->END.wrField(&AMACv2Reg::CntSetHV2en,0);	
  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV2en,0);
  m_amactb->END.wrField(&AMACv2Reg::CntSetHV3en,0);	
  m_amactb->END.wrField(&AMACv2Reg::CntSetCHV3en,0);

  // Power-off HV block
  m_amactb->setDAC(m_amactb->VDDHI_ADJ, 0.);

}

void AMACTest::readADCsAndAMChannels(const std::string& suffix){

  std::ofstream fh;
  fh.open(m_name+"_ReadAllAMChansAndADCs"+suffix+".csv");

  // AM channels
  fh << "VDCDC\tVDDLR\tDCDCin\tVDDREG\tsyst_BG_out\tAM900BG\tAM600BG\tCAL\tAMintref\tCALx\tCALy\tShuntx\tShunty\tDCDCadj\tDie_temp\tNTCxp,n\tNTCyp,n\tNTCpbp,n\tHrefx\tHrefy\tidcdcCMOut\tidcdcVlowTP\tidcdcVhighTP\todcdcCMOut\todcdcVlowTP\todcdcVhighTP\tHVret\tPTAT\t";
  
  // ADC measurements
  fh << "ADC_AM_VDD_V\tADC_AM_VDDLR_A\tADC_AM_VDDLR_V\tADC_AM_VDCDC_A\tADC_AM_VDCDC_V\tADC_AM_VDD_HI_A\tADC_AM_VDD_HI_V\tADC_Ext_NTC_ADC1\tADC_AM600BG\tADC_AM900BG\tADC_Shunty\tADC_Shuntx\tADC_CALy\tADC_CALx\tADC_AM_LVDS_CM0\tADC_AM_LVDS_CM1\tADC_CP_HVCtrl0\tADC_CP_HVCtrl1\tADC_CP_HVCtrl2\tADC_CP_HVCtrl3\tADC_AVCC_V\tADC_AVDD5_V\tADC_VCC5_V\tADC_VDD2V5_V\tADC_AVEE_V\tADC_VEE5_V\tADC_VDD1V2_V\tADC_AVCC_A\tADC_VCC5_A\tADC_HVret1\tADC_HVret2"  << std::endl;

  unsigned int initialMuxReg = m_amactb->END.read_reg(53);
  std::cout << "read: Register " << std::dec << 53 << " is: 0x" << std::hex << initialMuxReg << std::endl;

  unsigned int nibbleAMIntCalib = (initialMuxReg & 0xFF000000);
  std::cout << "nibble for AM int calib is: 0x" << std::hex << nibbleAMIntCalib << std::endl;

  // Set MUX to read all of the "channel a" values (AM nibble | 0x00000000)
  unsigned int muxValChanA = nibbleAMIntCalib;
  m_amactb->END.write_reg(53,muxValChanA);
  std::cout << "setting MUX to 0x" << std::hex << muxValChanA << std::endl;
  usleep(5e3); // note that the sleep is needed to give time for the MUX to work!

  int ch0 = m_amactb->END.rdField(&AMACv2Reg::Ch0Value);
  int ch1 = m_amactb->END.rdField(&AMACv2Reg::Ch1Value);
  int ch2 = m_amactb->END.rdField(&AMACv2Reg::Ch2Value);

  int ch3a = m_amactb->END.rdField(&AMACv2Reg::Ch3Value);
  int ch4a = m_amactb->END.rdField(&AMACv2Reg::Ch4Value);
  int ch5a = m_amactb->END.rdField(&AMACv2Reg::Ch5Value);

  int ch6 = m_amactb->END.rdField(&AMACv2Reg::Ch6Value);
  int ch7 = m_amactb->END.rdField(&AMACv2Reg::Ch7Value);
  int ch8 = m_amactb->END.rdField(&AMACv2Reg::Ch8Value);

  int ch9  = m_amactb->END.rdField(&AMACv2Reg::Ch9Value);
  int ch10 = m_amactb->END.rdField(&AMACv2Reg::Ch10Value);
  int ch11 = m_amactb->END.rdField(&AMACv2Reg::Ch11Value);

  int ch12a = m_amactb->END.rdField(&AMACv2Reg::Ch12Value);
  int ch13a = m_amactb->END.rdField(&AMACv2Reg::Ch13Value);
  int ch14  = m_amactb->END.rdField(&AMACv2Reg::Ch14Value);

  int ch15 = m_amactb->END.rdField(&AMACv2Reg::Ch15Value);

  // set the MUX to read all of the "channel b" values
  unsigned int muxValChanB = nibbleAMIntCalib | 0x111110;
  m_amactb->END.write_reg(53,muxValChanB);
  std::cout << "setting MUX to 0x" << std::hex << muxValChanB << std::endl;
  usleep(5e3);

  int ch3b = m_amactb->END.rdField(&AMACv2Reg::Ch3Value);
  int ch4b = m_amactb->END.rdField(&AMACv2Reg::Ch4Value);
  int ch5b = m_amactb->END.rdField(&AMACv2Reg::Ch5Value);

  int ch12b = m_amactb->END.rdField(&AMACv2Reg::Ch12Value);
  int ch13b = m_amactb->END.rdField(&AMACv2Reg::Ch13Value);

  // set the MUX to read all of the "channel c" values
  unsigned int muxValChanC = nibbleAMIntCalib | 0x222220;
  m_amactb->END.write_reg(53,muxValChanC);
  std::cout << "setting MUX to 0x" << std::hex << muxValChanC << std::endl;
  usleep(5e3);

  int ch3c = m_amactb->END.rdField(&AMACv2Reg::Ch3Value);
  int ch4c = m_amactb->END.rdField(&AMACv2Reg::Ch4Value);
  int ch5c = m_amactb->END.rdField(&AMACv2Reg::Ch5Value);

  int ch12c = m_amactb->END.rdField(&AMACv2Reg::Ch12Value);
  int ch13c = m_amactb->END.rdField(&AMACv2Reg::Ch13Value);

  // set the MUX to read all of the "channel d" values
  unsigned int muxValChanD = nibbleAMIntCalib | 0x3000;
  m_amactb->END.write_reg(53,muxValChanD);
  std::cout << "setting MUX to 0x" << std::hex << muxValChanD << std::endl;
  usleep(5e3);

  int ch5d = m_amactb->END.rdField(&AMACv2Reg::Ch5Value);

  // set the MUX to read all of the "channel e" values
  unsigned int muxValChanE = nibbleAMIntCalib | 0x4000;
  m_amactb->END.write_reg(53,muxValChanE);
  std::cout << "setting MUX to 0x" << std::hex << muxValChanE << std::endl;
  usleep(5e3);

  int ch5e = m_amactb->END.rdField(&AMACv2Reg::Ch5Value);

  // Printouts aren't necessary, but they're nice to be able to glance at
  std::cout << "ch0:   " << std::dec << ch0 << " (VDCDC)" << std::endl;
  std::cout << "ch1:   " << std::dec << ch1 << " (VDDLR)" << std::endl;
  std::cout << "ch2:   " << std::dec << ch2 << " (DCDCin)" << std::endl;
  std::cout << "ch3a:  " << std::dec << ch3a << " (VDDREG)" << std::endl;
  std::cout << "ch3b:  " << std::dec << ch3b << " (output of system bandgap)" << std::endl;
  std::cout << "ch3c:  " << std::dec << ch3c << " (AM900BG)" << std::endl;
  std::cout << "ch4a:  " << std::dec << ch4a << " (AM600BG)" << std::endl;
  std::cout << "ch4b:  " << std::dec << ch4b << " (CAL)" << std::endl;
  std::cout << "ch4c:  " << std::dec << ch4c << " (AM integrator reference)" << std::endl;
  std::cout << "ch5a:  " << std::dec << ch5a << " (CALx)" << std::endl;
  std::cout << "ch5b:  " << std::dec << ch5b << " (CALy)" << std::endl;
  std::cout << "ch5c:  " << std::dec << ch5c << " (Shuntx)" << std::endl;
  std::cout << "ch5d:  " << std::dec << ch5d << " (Shunty)" << std::endl;
  std::cout << "ch5e:  " << std::dec << ch5e << " (DCDCadj, but is wired to ground)" << std::endl;
  std::cout << "ch6:   " << std::dec << ch6 << " (Die temperature)" << std::endl;
  std::cout << "ch7:   " << std::dec << ch7 << " (NTCxp,n)" << std::endl;
  std::cout << "ch8:   " << std::dec << ch8 << " (NTCyp,n)" << std::endl;
  std::cout << "ch9:   " << std::dec << ch9 << " (NTCpbp,n)" << std::endl;
  std::cout << "ch10:  " << std::dec << ch10 << " (Hrefx)" << std::endl;
  std::cout << "ch11:  " << std::dec << ch11 << " (Hrefy)" << std::endl;
  std::cout << "ch12a: " << std::dec << ch12a << " (idcdcCMOut)" << std::endl;
  std::cout << "ch12b: " << std::dec << ch12b << " (idcdcVlowTP)" << std::endl;
  std::cout << "ch12c: " << std::dec << ch12c << " (idcdcVhighTP)" << std::endl;
  std::cout << "ch13a: " << std::dec << ch13a << " (odcdcCMOut)" << std::endl;
  std::cout << "ch13b: " << std::dec << ch13b << " (odcdcVlowTP)" << std::endl;
  std::cout << "ch13c: " << std::dec << ch13c << " (odcdcVhighTP)" << std::endl;
  std::cout << "ch14:  " << std::dec << ch14 << " (HVret)" << std::endl;
  std::cout << "ch15:  " << std::dec << ch15 << " (PTAT)" << std::endl;

  std::cout << std::endl;

  // Write AMAC channels to file
  fh << std::dec;
  fh << ch0 << "\t" << ch1 << "\t" << ch2 << "\t" << ch3a << "\t" << ch3b << "\t" << ch3c << "\t" << ch4a << "\t" << ch4b << "\t" << ch4c << "\t" << ch5a << "\t" << ch5b << "\t" << ch5c << "\t" << ch5d << "\t" << ch5e << "\t" << ch6 << "\t" << ch7 << "\t" << ch8 << "\t" << ch9 << "\t" << ch10 << "\t" << ch11 << "\t" << ch12a << "\t" << ch12b << "\t" << ch12c << "\t" << ch13a << "\t" << ch13b << "\t" << ch13c << "\t" << ch14 << "\t" << ch15 << "\t";

  // Write board ADC values to file
  fh << m_amactb->getADC(m_amactb->AM_VDD_V) << "\t";
  fh << m_amactb->getADC(m_amactb->AM_VDDLR_A) << "\t";
  fh << m_amactb->getADC(m_amactb->AM_VDDLR_V) << "\t";
  fh << m_amactb->getADC(m_amactb->AM_VDCDC_A) << "\t";
  fh << m_amactb->getADC(m_amactb->AM_VDCDC_V) << "\t";
  fh << m_amactb->getADC(m_amactb->AM_VDD_HI_A) << "\t";
  fh << m_amactb->getADC(m_amactb->AM_VDD_HI_V) << "\t";
  fh << m_amactb->getADC(m_amactb->Ext_NTC_ADC1) << "\t";
  fh << m_amactb->getADC(m_amactb->AM600BG) << "\t";
  fh << m_amactb->getADC(m_amactb->AM900BG) << "\t";
  fh << m_amactb->getADC(m_amactb->Shunty) << "\t";
  fh << m_amactb->getADC(m_amactb->Shuntx) << "\t";
  fh << m_amactb->getADC(m_amactb->CALy) << "\t";
  fh << m_amactb->getADC(m_amactb->CALx) << "\t";
  fh << m_amactb->getADC(m_amactb->AM_LVDS_CM0) << "\t";
  fh << m_amactb->getADC(m_amactb->AM_LVDS_CM1) << "\t";
  fh << m_amactb->getADC(m_amactb->CP_HVCtrl0) << "\t";
  fh << m_amactb->getADC(m_amactb->CP_HVCtrl1) << "\t";
  fh << m_amactb->getADC(m_amactb->CP_HVCtrl2) << "\t";
  fh << m_amactb->getADC(m_amactb->CP_HVCtrl3) << "\t";
  fh << m_amactb->getADC(m_amactb->AVCC_V) << "\t";
  fh << m_amactb->getADC(m_amactb->AVDD5_V) << "\t";
  fh << m_amactb->getADC(m_amactb->VCC5_V) << "\t";
  fh << m_amactb->getADC(m_amactb->VDD2V5_V) << "\t";
  fh << m_amactb->getADC(m_amactb->AVEE_V) << "\t";
  fh << m_amactb->getADC(m_amactb->VEE5_V) << "\t";
  fh << m_amactb->getADC(m_amactb->VDD1V2_V) << "\t";
  fh << m_amactb->getADC(m_amactb->AVCC_A) << "\t";
  fh << m_amactb->getADC(m_amactb->VCC5_A) << "\t";
  fh << m_amactb->getADC(m_amactb->HVret1) << "\t";
  fh << m_amactb->getADC(m_amactb->HVret2) << std::endl;

  m_amactb->END.write_reg(53,initialMuxReg);
  std::cout << "setting MUX back to its initial value " << initialMuxReg << std::endl;
  usleep(5e3);

  fh.close();
  return;
}
