# Package IP for a Zynq project.
#
# The resulting package is stored inside `${user_up_repo}/name`.
#
# Assumes that the vendor is LBL with the library
# being pbv3.
#
# Parameters
#  - name: comupter name of IP
#  - title: human readable name of IP
#  - description: description of IP purpose
#
proc build_zynq_ip {name title description} {
    puts "################################################################################"
    puts "## Generating ${title} IP"
    puts "################################################################################"

    ipx::package_project -root_dir $globalSettings::user_ip_repo/${name} -vendor lbl.gov -library pbv3 -taxonomy pbv3 -import_files

    set_property supported_families {zynq Production} [ipx::current_core]
    set_property name ${name} [ipx::current_core]
    set_property display_name ${title} [ipx::current_core]
    set_property description ${description} [ipx::current_core]
    set_property vendor_display_name {Lawrence Berkeley National Laboratory} [ipx::current_core]

    update_compile_order -fileset sources_1

    ipx::update_source_project_archive -component [ipx::current_core]

    ipx::save_core [ipx::current_core]
    update_ip_catalog
    ipx::create_xgui_files [ipx::current_core]
    ipx::update_checksums [ipx::current_core]
    ipx::save_core [ipx::current_core]
    ipx::unload_core [ipx::current_core]
}
