library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

----
-- Entity: simplescope
-- Author: Karol Krizka <kkrizka@gmail.com>
--
-- A simple oscillioscope for monitoring the status of the `input` logic
-- port. It counts the number of transitions (0->1 or 1->0) and stores
-- the last +/- 16 samplings around a transition.
--
-- The block can be reset using the `reset` signal. The input is sampled
-- using `clock`.
----
entity simplescope is
  port (
    reset       : in  std_logic;
    clock       : in  std_logic;
    input       : in  std_logic;

    counter     : out unsigned(31 downto 0); 
    trace       : out std_logic_vector(31 downto 0)
  );
end simplescope;

architecture arch_imp of simplescope is
  signal reg_input0     : std_logic;
  signal reg_input      : std_logic;
  
  signal reg_counter    : unsigned(31 downto 0);
  signal reg_trace      : std_logic_vector(31 downto 0);
  signal reg_fr_trace   : std_logic_vector(31 downto 0);
begin
  counter       <= reg_counter;
  trace         <= reg_fr_trace;

  proc_input_reg: process (clock)
  begin
    if rising_edge(clock) then
      reg_input0        <= input;
      reg_input         <= reg_input0;
    end if;
  end process;

  proc_scope    : process (reset, clock)
  begin
    if rising_edge(clock) then
      if reset = '1' then
        reg_counter             <= to_unsigned(0, 32);
        reg_trace               <= (others => reg_input);
        reg_fr_trace            <= (others => '0');
      else
        reg_trace(31 downto 1)    <= reg_trace(30 downto 0);
        reg_trace(0)              <= reg_input;
        if reg_trace(15) /= reg_trace(16) then
          reg_counter             <= reg_counter + 1;
          reg_fr_trace            <= reg_trace;
        end if;
      end if;
    end if;
  end process;
end arch_imp;
