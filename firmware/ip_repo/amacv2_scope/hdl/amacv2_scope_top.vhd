library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

----
-- Entity: amacv2_scope
-- Author: Karol Krizka <kkrizka@gmail.com>
--
-- Top-level entity for an AXI-enabled block for scoping the digital output signals
-- of the AMAC.
--
-- The following AXI registers are available:
--  0x00        : reset all scopes by writing to bit 1
--  0x02+2*ch   : counter from scope at channel ch
--  0x02+2*ch+1 : last trace from channel ch, frozen with transition at center
--
-- The following signals are monitored:
--  ch : signal
--   0 : LDx0en
--   1 : LDy0en
--   2 : LDx1en
--   3 : LDy1en
--   4 : LDx2en
--   5 : LDy2en
--   6 : HrstBx
--   7 : HrstBy
--   8 : LAM
--   9 : GPO
--  10 : DCDCadj
--  11 : DCDCEn
--  12 : Ofout
--  13 : RO_PG_O
----
entity amacv2_scope is
  generic (
    -- Users to add parameters here

    -- User parameters ends
    -- Do not modify the parameters beyond this line

    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH	: integer	:= 32;
    C_S00_AXI_ADDR_WIDTH	: integer	:= 7
    );
  port (
    -- Users to add ports here

    -- AMAC outputs
    LDx0en  : in std_logic;
    LDy0en  : in std_logic;
    LDx1en  : in std_logic;
    LDy1en  : in std_logic;
    LDx2en  : in std_logic;
    LDy2en  : in std_logic;
    HrstBx  : in std_logic;
    HrstBy  : in std_logic;
    LAM     : in std_logic;
    GPO     : in std_logic;
    DCDCadj : in std_logic;
    DCDCEn  : in std_logic;
    Ofout   : in std_logic;
    RO_PG_O : in std_logic;

    -- Do not modify the ports beyond this line


    -- Ports of Axi Slave Bus Interface S00_AXI
    s00_axi_aclk	: in std_logic;
    s00_axi_aresetn	: in std_logic;
    s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_awprot	: in std_logic_vector(2 downto 0);
    s00_axi_awvalid	: in std_logic;
    s00_axi_awready	: out std_logic;
    s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s00_axi_wvalid	: in std_logic;
    s00_axi_wready	: out std_logic;
    s00_axi_bresp	: out std_logic_vector(1 downto 0);
    s00_axi_bvalid	: out std_logic;
    s00_axi_bready	: in std_logic;
    s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_arprot	: in std_logic_vector(2 downto 0);
    s00_axi_arvalid	: in std_logic;
    s00_axi_arready	: out std_logic;
    s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_rresp	: out std_logic_vector(1 downto 0);
    s00_axi_rvalid	: out std_logic;
    s00_axi_rready	: in std_logic
    );
end amacv2_scope;

architecture arch_imp of amacv2_scope is

  -- component declaration
  component generic_read_registers_AXI is
    generic (
      C_S_AXI_DATA_WIDTH	: integer	:= 32;
      C_S_AXI_ADDR_WIDTH	: integer	:= 7
      );
    port (
      reset             : out std_logic_vector(31 downto 0);
      control           : out std_logic_vector(31 downto 0);
      reg2              : in  std_logic_vector(31 downto 0);
      reg3              : in  std_logic_vector(31 downto 0);
      reg4              : in  std_logic_vector(31 downto 0);
      reg5              : in  std_logic_vector(31 downto 0);
      reg6              : in  std_logic_vector(31 downto 0);
      reg7              : in  std_logic_vector(31 downto 0);
      reg8              : in  std_logic_vector(31 downto 0);
      reg9              : in  std_logic_vector(31 downto 0);
      reg10             : in  std_logic_vector(31 downto 0);
      reg11             : in  std_logic_vector(31 downto 0);
      reg12             : in  std_logic_vector(31 downto 0);
      reg13             : in  std_logic_vector(31 downto 0);
      reg14             : in  std_logic_vector(31 downto 0);
      reg15             : in  std_logic_vector(31 downto 0);
      reg16             : in  std_logic_vector(31 downto 0);
      reg17             : in  std_logic_vector(31 downto 0);
      reg18             : in  std_logic_vector(31 downto 0);
      reg19             : in  std_logic_vector(31 downto 0);
      reg20             : in  std_logic_vector(31 downto 0);
      reg21             : in  std_logic_vector(31 downto 0);
      reg22             : in  std_logic_vector(31 downto 0);
      reg23             : in  std_logic_vector(31 downto 0);
      reg24             : in  std_logic_vector(31 downto 0);
      reg25             : in  std_logic_vector(31 downto 0);
      reg26             : in  std_logic_vector(31 downto 0);
      reg27             : in  std_logic_vector(31 downto 0);
      reg28             : in  std_logic_vector(31 downto 0);
      reg29             : in  std_logic_vector(31 downto 0);
      reg30             : in  std_logic_vector(31 downto 0);
      reg31             : in  std_logic_vector(31 downto 0);
      S_AXI_ACLK	: in  std_logic;
      S_AXI_ARESETN	: in  std_logic;
      S_AXI_AWADDR	: in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_AWPROT	: in  std_logic_vector(2 downto 0);
      S_AXI_AWVALID	: in  std_logic;
      S_AXI_AWREADY	: out std_logic;
      S_AXI_WDATA	: in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_WSTRB	: in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
      S_AXI_WVALID	: in  std_logic;
      S_AXI_WREADY	: out std_logic;
      S_AXI_BRESP	: out std_logic_vector(1 downto 0);
      S_AXI_BVALID	: out std_logic;
      S_AXI_BREADY	: in  std_logic;
      S_AXI_ARADDR	: in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
      S_AXI_ARPROT	: in  std_logic_vector(2 downto 0);
      S_AXI_ARVALID	: in  std_logic;
      S_AXI_ARREADY	: out std_logic;
      S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      S_AXI_RRESP	: out std_logic_vector(1 downto 0);
      S_AXI_RVALID	: out std_logic;
      S_AXI_RREADY	: in  std_logic
      );
  end component generic_read_registers_AXI;

  component simplescope is
  port (
    reset       : in  std_logic;
    clock       : in  std_logic;
    input       : in  std_logic;
    counter     : out unsigned(31 downto 0); 
    trace       : out std_logic_vector(31 downto 0)
  );
  end component simplescope;

  --
  -- Signals
  signal reset_full     : std_logic_vector(31 downto 0);
  signal scope_reset    : std_logic;
  

  signal counter_LDx0en : unsigned(31 downto 0);
  signal trace_LDx0en   : std_logic_vector(31 downto 0);
  signal counter_LDy0en : unsigned(31 downto 0);        
  signal trace_LDy0en   : std_logic_vector(31 downto 0);
  signal counter_LDx1en : unsigned(31 downto 0);        
  signal trace_LDx1en   : std_logic_vector(31 downto 0);
  signal counter_LDy1en : unsigned(31 downto 0);        
  signal trace_LDy1en   : std_logic_vector(31 downto 0);
  signal counter_LDx2en : unsigned(31 downto 0);        
  signal trace_LDx2en   : std_logic_vector(31 downto 0);
  signal counter_LDy2en : unsigned(31 downto 0);        
  signal trace_LDy2en   : std_logic_vector(31 downto 0);
  signal counter_HrstBx : unsigned(31 downto 0);        
  signal trace_HrstBx   : std_logic_vector(31 downto 0);
  signal counter_HrstBy : unsigned(31 downto 0);        
  signal trace_HrstBy   : std_logic_vector(31 downto 0);
  signal counter_LAM    : unsigned(31 downto 0);        
  signal trace_LAM      : std_logic_vector(31 downto 0);
  signal counter_GPO    : unsigned(31 downto 0);        
  signal trace_GPO      : std_logic_vector(31 downto 0);
  signal counter_DCDCadj: unsigned(31 downto 0);        
  signal trace_DCDCadj  : std_logic_vector(31 downto 0);
  signal counter_DCDCEn : unsigned(31 downto 0);        
  signal trace_DCDCEn   : std_logic_vector(31 downto 0);
  signal counter_Ofout  : unsigned(31 downto 0);        
  signal trace_Ofout    : std_logic_vector(31 downto 0);
  signal counter_RO_PG_O: unsigned(31 downto 0);        
  signal trace_RO_PG_O  : std_logic_vector(31 downto 0);
begin
  --
  -- AMAC output scopes
  scope_LDx0en_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => LDx0en,
      counter   => counter_LDx0en,
      trace     => trace_LDx0en
      );

  scope_LDy0en_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => LDy0en,
      counter   => counter_LDy0en,
      trace     => trace_LDy0en
      );

  scope_LDx1en_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => LDx1en,
      counter   => counter_LDx1en,
      trace     => trace_LDx1en
      );

  scope_LDy1en_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => LDy1en,
      counter   => counter_LDy1en,
      trace     => trace_LDy1en
      );

  scope_LDx2en_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => LDx2en,
      counter   => counter_LDx2en,
      trace     => trace_LDx2en
      );

  scope_LDy2en_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => LDy2en,
      counter   => counter_LDy2en,
      trace     => trace_LDy2en
      );

  scope_HrstBx_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => HrstBx,
      counter   => counter_HrstBx,
      trace     => trace_HrstBx
      );

  scope_HrstBy_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => HrstBy,
      counter   => counter_HrstBy,
      trace     => trace_HrstBy
      );

  scope_LAM_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => LAM,
      counter   => counter_LAM,
      trace     => trace_LAM
      );

  scope_GPO_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => GPO,
      counter   => counter_GPO,
      trace     => trace_GPO
      );

  scope_DCDCadj_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => DCDCadj,
      counter   => counter_DCDCadj,
      trace     => trace_DCDCadj
      );

  scope_DCDCEn_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => DCDCEn,
      counter   => counter_DCDCEn,
      trace     => trace_DCDCEn
      );

  scope_Ofout_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => Ofout,
      counter   => counter_Ofout,
      trace     => trace_Ofout
      );

  scope_RO_PG_O_inst : simplescope
    port map (
      clock     => s00_axi_aclk,
      reset     => scope_reset,
      input     => RO_PG_O,
      counter   => counter_RO_PG_O,
      trace     => trace_RO_PG_O
      );

  --
  -- Instantiation of Axi Bus Interface AXI
  generic_read_registers_AXI_inst : generic_read_registers_AXI
    generic map (
      C_S_AXI_DATA_WIDTH=> C_S00_AXI_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH=> C_S00_AXI_ADDR_WIDTH
      )
    port map (
      reset             => reset_full,
      reg2              => std_logic_vector(counter_LDx0en),
      reg3              => trace_LDx0en,
      reg4              => std_logic_vector(counter_LDy0en),
      reg5              => trace_LDy0en,
      reg6              => std_logic_vector(counter_LDx1en),
      reg7              => trace_LDx1en,
      reg8              => std_logic_vector(counter_LDy1en),
      reg9              => trace_LDy1en,
      reg10             => std_logic_vector(counter_LDx2en),
      reg11             => trace_LDx2en,
      reg12             => std_logic_vector(counter_LDy2en),
      reg13             => trace_LDy2en,
      reg14             => std_logic_vector(counter_HrstBx),
      reg15             => trace_HrstBx,
      reg16             => std_logic_vector(counter_HrstBy),
      reg17             => trace_HrstBy,
      reg18             => std_logic_vector(counter_LAM),
      reg19             => trace_LAM,
      reg20             => std_logic_vector(counter_GPO),
      reg21             => trace_GPO,
      reg22             => std_logic_vector(counter_DCDCadj),
      reg23             => trace_DCDCadj,
      reg24             => std_logic_vector(counter_DCDCEn),
      reg25             => trace_DCDCEn,
      reg26             => std_logic_vector(counter_Ofout),
      reg27             => trace_Ofout,
      reg28             => std_logic_vector(counter_RO_PG_O),
      reg29             => trace_RO_PG_O,
      reg30             => (others => '0'),
      reg31             => (others => '0'),
      S_AXI_ACLK	=> s00_axi_aclk,
      S_AXI_ARESETN	=> s00_axi_aresetn,
      S_AXI_AWADDR	=> s00_axi_awaddr,
      S_AXI_AWPROT	=> s00_axi_awprot,
      S_AXI_AWVALID	=> s00_axi_awvalid,
      S_AXI_AWREADY	=> s00_axi_awready,
      S_AXI_WDATA	=> s00_axi_wdata,
      S_AXI_WSTRB	=> s00_axi_wstrb,
      S_AXI_WVALID	=> s00_axi_wvalid,
      S_AXI_WREADY	=> s00_axi_wready,
      S_AXI_BRESP	=> s00_axi_bresp,
      S_AXI_BVALID	=> s00_axi_bvalid,
      S_AXI_BREADY	=> s00_axi_bready,
      S_AXI_ARADDR	=> s00_axi_araddr,
      S_AXI_ARPROT	=> s00_axi_arprot,
      S_AXI_ARVALID	=> s00_axi_arvalid,
      S_AXI_ARREADY	=> s00_axi_arready,
      S_AXI_RDATA	=> s00_axi_rdata,
      S_AXI_RRESP	=> s00_axi_rresp,
      S_AXI_RVALID	=> s00_axi_rvalid,
      S_AXI_RREADY	=> s00_axi_rready
      );

  -- Add user logic here
  scope_reset   <= reset_full(0);
  -- User logic ends

end arch_imp;
