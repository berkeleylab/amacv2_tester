################################################################
# CONFIGURE
################################################################

variable script_folder
set script_folder ${repo_path}/bd

################################################################
# START
################################################################

# Procedure to create the entire design
#
# Creates:
#  - processing system IP
#  - AXI control and related reset procedures
#  - endeavour IP for AMAC communication
#  - SPI IP for active board control
#
# @param parentCell If "", will use root.
#
proc create_root_design { parentCell } {

    variable script_folder

    if { $parentCell eq "" } {
	set parentCell [get_bd_cells /]
    }

    # Get object for parentCell
    set parentObj [get_bd_cells $parentCell]
    if { $parentObj == "" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
	return
    }

    # Make sure parentObj is hier blk
    set parentType [get_property TYPE $parentObj]
    if { $parentType ne "hier" } {
	catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
	return
    }

    # Save current instance; Restore later
    set oldCurInst [current_bd_instance .]

    # Set parent object as current
    current_bd_instance $parentObj

    #
    # Create Processing System and corresponding AXI bus

    # Processing System
    set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
    apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 \
	-config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" } \
	[get_bd_cells processing_system7_0]
    set_property -dict [list \
			    CONFIG.PCW_USE_M_AXI_GP0 {1} \
			    CONFIG.PCW_USE_S_AXI_HP0 {0} \
			    CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
			    CONFIG.PCW_IRQ_F2P_INTR {1} \
			   ]  ${processing_system7_0}

    # AXI
    set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0 ]
    set_property -dict [ list \
    			     CONFIG.NUM_MI 5 \
    			    ] $axi_interconnect_0

    connect_bd_intf_net [get_bd_intf_pins processing_system7_0/M_AXI_GP0] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
	-config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
	[get_bd_pins axi_interconnect_0/ACLK]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
	-config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
	[get_bd_pins axi_interconnect_0/S00_ACLK]

    #
    # AMACv2 IO

    # IP
    set axi_amacv2_digital_io_0 [create_bd_cell -type ip -vlnv lbl.gov:pbv3:amacv2_digital_io:1.0 axi_amacv2_digital_io_0]

    # Ports
    set amac_inputs  [list SSSHx SSSHy RESETB PGOOD GPI OFin LD_EN_VDDRL LV_EN_2V5 LV_EN_VP5 LV_EN_VN5 LV_EN_AVDD5 LV_EN_AVEE LV_EN_AVCC ADC_CNV LD_EN_VDCDC HVret_SW LVL_TRANS_EN MPM_MUX_EN HVSW_MUX_EN FPGA_EFUSE_PULSE HVref_HGND_SW]
    set amac_outputs [list LDx0en LDx1en LDx2en LDy0en LDy1en LDy2en HrstBx HrstBy LAM GPO DCDCadj DCDCen OFout RO_PG_O]

    foreach amac_output ${amac_outputs} {
	create_bd_port -dir I ${amac_output}
	connect_bd_net [get_bd_ports ${amac_output}] [get_bd_pins ${axi_amacv2_digital_io_0}/${amac_output}]
    }

    foreach amac_input ${amac_inputs} {
	create_bd_port -dir O ${amac_input}
	connect_bd_net [get_bd_ports ${amac_input}] [get_bd_pins ${axi_amacv2_digital_io_0}/${amac_input}]
    }

    create_bd_port -dir O -from 4 -to 0 ID
    connect_bd_net [get_bd_ports ID] [get_bd_pins ${axi_amacv2_digital_io_0}/ID]

    create_bd_port -dir O -from 2 -to 0 MPM_MUX
    connect_bd_net [get_bd_ports MPM_MUX] [get_bd_pins ${axi_amacv2_digital_io_0}/MUX_SEL]

    # AXI
    connect_bd_intf_net [get_bd_intf_pins ${axi_amacv2_digital_io_0}/s00_axi] [get_bd_intf_pins axi_interconnect_0/M00_AXI]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
        -config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
        [get_bd_pins axi_interconnect_0/M00_ACLK]

    assign_bd_address -offset 0x41C10000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs ${axi_amacv2_digital_io_0}/s00_axi/reg0] -force

    #
    # Endeavour

    # IP
    set axi_endeavour_0 [create_bd_cell -type ip -vlnv lbl.gov:pbv3:endeavour:1.0 axi_endeavour_0]

    # Ports
    create_bd_port -dir O CMD_IN_N
    create_bd_port -dir O CMD_IN_P
    create_bd_port -dir I CMD_OUT_N
    create_bd_port -dir I CMD_OUT_P
    create_bd_port -dir O LED0
    create_bd_port -dir O LED1
    create_bd_port -dir O LED2
    create_bd_port -dir O LED3

    connect_bd_net [get_bd_ports CMD_OUT_N] [get_bd_pins ${axi_endeavour_0}/CMD_OUT_N]
    connect_bd_net [get_bd_ports CMD_OUT_P] [get_bd_pins ${axi_endeavour_0}/CMD_OUT_P]
    connect_bd_net [get_bd_ports CMD_IN_N ] [get_bd_pins ${axi_endeavour_0}/CMD_IN_N ]
    connect_bd_net [get_bd_ports CMD_IN_P ] [get_bd_pins ${axi_endeavour_0}/CMD_IN_P ]

    connect_bd_net [get_bd_ports LED0] [get_bd_pins ${axi_endeavour_0}/busy ]
    connect_bd_net [get_bd_ports LED1] [get_bd_pins ${axi_endeavour_0}/datavalid ]
    connect_bd_net [get_bd_ports LED2] [get_bd_pins ${axi_endeavour_0}/error ]

    # AXI
    connect_bd_intf_net [get_bd_intf_pins ${axi_endeavour_0}/s00_axi] [get_bd_intf_pins axi_interconnect_0/M01_AXI]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
        -config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
        [get_bd_pins axi_interconnect_0/M01_ACLK]

    assign_bd_address -offset 0x41C00000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs ${axi_endeavour_0}/s00_axi/reg0] -force

    #
    # SPI

    # IP
    set axi_quad_spi_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 axi_quad_spi_0 ]
    set_property -dict [ list \
    			     CONFIG.C_USE_STARTUP {0} \
			     CONFIG.C_NUM_SS_BITS {8} \
    			    ] $axi_quad_spi_0

    # Ports
    create_bd_intf_port -mode Master -vlnv xilinx.com:interface:spi_rtl:1.0 spi_rtl
    connect_bd_intf_net [get_bd_intf_ports spi_rtl] [get_bd_intf_pins axi_quad_spi_0/SPI_0]

    # AXI
    connect_bd_intf_net [get_bd_intf_pins axi_quad_spi_0/AXI_LITE] [get_bd_intf_pins axi_interconnect_0/M02_AXI]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
        -config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
        [get_bd_pins axi_interconnect_0/M02_ACLK]

    assign_bd_address -offset 0x41E00000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_quad_spi_0/AXI_LITE/Reg] -force

    # Connections
    connect_bd_net [get_bd_pins axi_quad_spi_0/ext_spi_clk] [get_bd_pins axi_quad_spi_0/s_axi_aclk]

    connect_bd_net [get_bd_pins axi_quad_spi_0/ip2intc_irpt] [get_bd_pins processing_system7_0/IRQ_F2P]

    #
    # Frequency Measurement

    # IP
    set axi_freqmeas_0 [create_bd_cell -type ip -vlnv lbl.gov:pbv3:freqmeas:1.0 axi_freqmeas_0]

    # Ports
    for {set i 0} {${i} < 4} {incr i} {
	create_bd_port -dir I HVOSC${i}
	connect_bd_net [get_bd_ports HVOSC${i}] [get_bd_pins ${axi_freqmeas_0}/frq${i}_i]
    }

    create_bd_port -dir I CLKOUT
    connect_bd_net [get_bd_ports CLKOUT] [get_bd_pins ${axi_freqmeas_0}/frq4_i]

    # AXI
    connect_bd_intf_net [get_bd_intf_pins ${axi_freqmeas_0}/s00_axi] [get_bd_intf_pins axi_interconnect_0/M03_AXI]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
        -config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
        [get_bd_pins axi_interconnect_0/M03_ACLK]

    assign_bd_address -offset 0x41C20000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs ${axi_freqmeas_0}/s00_axi/reg0] -force

    #
    # AMACv2 Output Scope

    # IP
    set axi_amacv2_scope_0 [create_bd_cell -type ip -vlnv lbl.gov:pbv3:amacv2_scope:1.0 axi_amacv2_scope_0]

    # Ports
    set amac_outputs [list LDx0en LDx1en LDx2en LDy0en LDy1en LDy2en HrstBx HrstBy LAM GPO DCDCadj DCDCen OFout RO_PG_O]

    foreach amac_output ${amac_outputs} {
    	# create_bd_port -dir I ${amac_output} # Already done as part of the IO controller
    	connect_bd_net [get_bd_ports ${amac_output}] [get_bd_pins ${axi_amacv2_scope_0}/${amac_output}]
    }

    # AXI
    connect_bd_intf_net [get_bd_intf_pins ${axi_amacv2_scope_0}/s00_axi] [get_bd_intf_pins axi_interconnect_0/M04_AXI]

    apply_bd_automation -rule xilinx.com:bd_rule:clkrst \
        -config { Clk {/processing_system7_0/FCLK_CLK0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}} \
        [get_bd_pins axi_interconnect_0/M04_ACLK]

    assign_bd_address -offset 0x41C30000 -range 0x00010000 -target_address_space [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs ${axi_amacv2_scope_0}/s00_axi/reg0] -force

    #
    # Cleanup

    # Restore current instance
    current_bd_instance $oldCurInst

    save_bd_design
}
