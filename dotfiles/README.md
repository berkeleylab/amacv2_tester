This directory contains various convenience dotfiles for us to use on Petalinux.
Feel free to add more as needed or make improvements to these.

To use these files, copy them into your home directory.
