# .bash_profile

# to autotabcomplete only dirs with cd
complete -d cd

# to solve http://unix.stackexchange.com/questions/61584/how-to-solve-the-issue-that-a-terminal-screen-is-messed-up-usually-after-a-res
shopt -s checkwinsize

# to solve https://stackoverflow.com/questions/6418493/bash-variable-expansion-on-tab-complete
shopt -s direxpand

# to use .inputrc file for easily finding old commands
bind -f /run/media/mmcblk0p2/home/root/.inputrc

# User specific environment and startup programs
export TZ=America/New_York
export TERM="screen-256color"
PS1="[\$(date +%H:%M)][microzed \W]\$ "

# History Stuff
export HISTFILESIZE=
export HISTSIZE=
HISTCONTROL=ignoreboth # Ignore duplicate commands and whitespace
export HISTIGNORE=ls # Also ignore ls
export HISTFILE=/run/media/mmcblk0p2/home/root/.bash_history
PROMPT_COMMAND='history -a' # to append to history after each command

# Read all history lines from HISTFILE and put them into the list
# used at the command line
history -n

# ls stuff
alias l='ls'
alias s='ls'
alias sl='ls'
alias ll='ls -l'

alias :q=''
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
