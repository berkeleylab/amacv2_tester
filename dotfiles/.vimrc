set shiftwidth=2
set softtabstop=2
set expandtab
"set smartindent
set autoindent
filetype plugin indent on
set hidden
set backspace=indent,eol,start
set incsearch
set hlsearch
set ruler
set mouse-=a
set noundofile

syntax enable

