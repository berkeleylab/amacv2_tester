source ${repo_path}/firmware/tcl/build_ip.tcl
build_zynq_ip freqmeas "Frequency Measurement" "Measure frequency of a signal with AXI readout."
