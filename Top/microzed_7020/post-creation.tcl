source ${repo_path}/firmware/bd/ActiveBoard.tcl

create_bd_design ActiveBoard
current_bd_design ActiveBoard

create_root_design {}

make_wrapper -files [get_files ActiveBoard.bd] -top -import
set_property top ActiveBoard_wrapper [current_fileset]
