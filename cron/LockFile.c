#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

//
// Note: some code borrowed from https://stackoverflow.com/a/1643134
// TODO: This code was also used in the AMACv1a irradiation (and HCC130 irradiation),
// so I'll port this into our current CMake-based builds soon.
//
// Currently one can build via `gcc LockFile.c -o LockFile`
//

/*! Try to get lock. Return its file descriptor or -1 if failed.
 *
 *  @param lockName Name of file used as lock (i.e. '/var/lock/myLock').
 *  @return File descriptor of lock file, or -1 if failed.
 */
int tryGetLock( char const *lockName )
{
    mode_t m = umask( 0 );
    int fd = open( lockName, O_RDWR|O_CREAT, 0666 );
    umask( m );
    if( fd >= 0 && flock( fd, LOCK_EX | LOCK_NB ) < 0 )
    {
        close( fd );
        fd = -1;
    }
    return fd;
}

/*! Release the lock obtained with tryGetLock( lockName ).
 *
 *  @param fd File descriptor of lock returned by tryGetLock( lockName ).
 *  @param lockName Name of file used as lock (i.e. '/var/lock/myLock').
 */
void releaseLock( int fd, char const *lockName )
{
    if( fd < 0 )
        return;
    remove( lockName );
    close( fd );
}


/*! Open lock file and run script when the lock is ready
 *
 */
int main(int argc, char *argv[])
{
  
  int fd = -1;
  int count = 0;
  int max = 2400; // 2400 seconds == 40 minutes

  // FIXME we'll need to set these paths somewhere...
  const char* FNAME = "/run/media/mmcblk0p2/home/root/dir_for_cron_tests/amacv2_tester/software/build/bin/.myLock";
  char path[] = "/run/media/mmcblk0p2/home/root/dir_for_cron_tests/amacv2_tester/software/build/bin/";

  char *full_path = malloc (strlen(path)+strlen(argv[1])+4);
  strcpy(full_path, path);
  strcat(full_path, argv[1]);

  while(fd == -1){

    if(count > max){
      printf("Unable to open lock file for the past %d minutes! Giving up now.\n", max/60);
      return 0;
    }

    fd = tryGetLock(FNAME);
    sleep(1);
    count++;
  }

  // run process here
  printf("about to run script\n");
  system (full_path);

  //unlink file
  releaseLock(fd, FNAME);   

  printf("done\n");
  full_path=NULL;
  free(full_path);

  return 0;
}
