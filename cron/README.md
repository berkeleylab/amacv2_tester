# Cron utilities for AMACv2 irradiation

## Compilation
Similar to including CMake in the Petalinux installation (see [here](https://gitlab.cern.ch/berkeleylab/amacv2_tester/blob/master/software/README.md)), cron must also be included when installing Petalinux.

To do this, we'll use [cronie](https://github.com/openembedded/openembedded-core/tree/master/meta/recipes-extended/cronie), which can be enabled by doing the following:

In `amacv2_testbench_microzed.linux/project-spec/meta-user/recipes-core/images/petalinux-image.bbappend`, add the line:

```
IMAGE_INSTALL_append = " cronie"
```

Then enable the package when running `petalinux-config -c rootfs`:
```
-> user packages
  -> [*] cronie
```

And then build petalinux as usual via `petalinux-build -p amacv2_testbench_microzed.linux`.


## Usage

* Modify the `cron.conf` file as needed, and then run `crontab cron.conf`.

* `crontab -l` can be used to determine what the currently loaded configuration looks like.

* Note that cron does not pick up the user's environment, so env vars may not be usable unless one directly specified them!

* Currently, one may need to run `crontab cron.conf` upon **each** restart!
